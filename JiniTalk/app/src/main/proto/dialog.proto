syntax = "proto3";

import "google/protobuf/empty.proto";
import "google/protobuf/timestamp.proto";
import "google/protobuf/duration.proto";
import "userattr.proto";

package elsa.facade;


message ServiceGroupDetail {
  bool auth = 1;
  string auth_provider = 2;

  string greeting = 11;
  string advertising = 12;
  map<string, string> icons = 13;
  string bg_color = 14;
  string bg_image = 15;
  repeated string tags = 16;
  repeated string categories = 17;
}

message ServiceGroup {
  string name = 1;
  string title = 2;
  string description = 3;
  bool is_default = 4;
  string version = 5;

  repeated DialogAgent agents = 100;
  ServiceGroupDetail detail = 200;
}

message ServiceGroupKey {
  string name = 1;
}

message ServiceGroupList {
  repeated ServiceGroup svc_groups = 1;
}

message DomainUserAttributes {
  string domain = 1; // domain name specified by admin
  string description = 2; // description by admin
  UserAttributeList user_attrs = 11;
}

message ServiceGroupUserAttributes {
  string svc_group = 1; // chatbot name specified by admin
  repeated DomainUserAttributes domain_user_attrs = 11;
}

//
// Service Group Finder
//
// Get information on service groups
// If you want to use authenticated mode,
// you can provide meta data with
// 'x-elsa-authentication-token' and
// 'x-elsa-authentication-provider'
service ServiceGroupFinder {
  // Get service group list
  rpc GetServiceGroups (google.protobuf.Empty) returns (ServiceGroupList) {
  }

  // Get a service group.
  rpc GetServiceGroup (ServiceGroupKey) returns (ServiceGroup) {
  }

  // Get attributes on service group
  rpc GetUserAttributes (ServiceGroupKey) returns (ServiceGroupUserAttributes) {
  }
}

///
/// DIALOG SERVICE
///

enum AccessFrom {
  ANONYMOUS = 0;
  WEB_BROSWER = 1;
  TEXT_MESSENGER = 2;
  MOBILE_ANDROID = 11;
  MOBILE_IPHONE = 12;
  SPEAKER = 20;
  TELEPHONE = 30;
  EXT_API = 1000;
}

message Caller {
  string svc_group = 1;
  string name = 2;
  string email = 3;
  string phone_no = 4;
  map<string, string> properties = 5;
  AccessFrom accessFrom = 6;
}

message DialogAgent {
  string name = 1;
  string lang = 2;
  string description = 3;
  string version = 4;
  string domain = 5;
  map<string, string> properties = 6;
}

/**
 *
 */
message Session {
  int64 id = 1;
  string svc_group = 2;
  string user_key = 3;
  AccessFrom accessFrom = 4;
  map<string, string> contexts = 5;
  google.protobuf.Timestamp start_time = 6;
  google.protobuf.Duration duration = 7;
  bool human_assisted = 8;
}

message SessionKey {
  int64 session_id = 1;
}

message AudioUtter {
  bytes utter = 1;
}

message TextUtter {
  string utter = 1;
}

message Image {
  bytes body = 1;
}

enum DialogResult {
  OK = 0;
  USER_INVLIDATED = 101;
  SERVICE_GROUP_NOT_FOUND = 102;
  SESSION_NOT_FOUND = 103;
  SESSION_INVLIDATED = 104;

  EMPTY_IN = 201;

  DOMAIN_CLASSIFIER_FAILED = 301;
  DIALOG_AGENT_NOT_FOUND = 302;
  DIALOG_AGENT_INVALIDATED = 303;
  LAST_DIALG_AGENT_LOST = 304;
  CHATBOT_NOT_FOUND = 305;
  HUMAN_AGENT_NOT_FOUND = 306;
  SECONDARY_DIALOG_CONFLICT = 311;
  LAST_AGENT_NOT_FOUND = 312;
  DUPLICATED_DOMAIN_RESOLVED = 313;
  SECONDARY_DIALOG_AGENT_IS_MULTI_TURN = 314;

  INTERNAL_ERROR = 500;
}

enum LangCode {
  kor = 0;
  eng = 1;
}

message SessionStat {
  int32 result_code = 1;
  string full_message = 2;
}


// 과거 세션 조회 조건
message PastSessionQuery {
  string user = 1;
  int64 id = 2;
  string svc_group = 3;
  AccessFrom access_from = 4;
  string peer_ip = 5;
  string sdate = 6;
  string edate = 7;

  int32 limit = 31;
  string page = 32;
}

// 과거 세션
message PastSession {
  int64 id = 1;
  AccessFrom access_from = 2;
  string peer_ip = 3;
  string sdate = 4;
  string stime = 5;
  string edate = 6;
  string etime = 7;
  string svc_group = 8;
  string user = 9;
  int32 talk_count = 10;
}

// 과거 세션 리스트
message PastSessionList {
  repeated PastSession sessions = 1;
  string page_state = 2;
}

// 과거 대화 조건
message PastTalkQuery {
  string user = 1;
  int64 sid = 2;
  string sdate = 3;
  string edate = 4;
  string domain = 5;
  int32 start_cl = 6;
  int32 end_cl = 7;
  string text = 8;

  int32 limit = 31;
  string page = 32;
}

// 과거 대화
message PastTalk {
  int64 sid = 1;
  int32 seq = 2;
  bool audio = 3;
  string audio_record_file = 4;
  int32 sample_rate = 5;
  bool image = 6;
  LangCode lang = 7;
  string domain = 8;
  string in_mangled = 9;
  string text = 10;
  string user = 11;
  string date = 12;
  string time = 13;
  map<string, string> meta = 14;
  map<string, string> intention = 15;
  map<string, string> context = 16;
  repeated string agent = 17;
  int32 type = 18;
  float cl_result = 19;
  int32 warned = 20;
}

// 과거 대화 리스트
message PastTalkList {
  repeated PastTalk talks = 1;
  string page_state = 2;
}

// Talk service with endpoint client
service DialogService {
  // Open a new session
  rpc Open (Caller) returns (Session) {
  }

  rpc AudioTalk (stream AudioUtter) returns (stream AudioUtter) {
  }

  rpc AudioToTextTalk (stream AudioUtter) returns (TextUtter) {
  }

  rpc TextTalk (stream TextUtter) returns (stream TextUtter) {
  }

  rpc ImageToTextTalk (stream Image) returns (stream TextUtter) {
  }

  // update current session. If not exist, NOT_FOUND returns.
  rpc UpdateSession (SessionKey) returns (Session) {
  }

  // close session
  rpc Close (SessionKey) returns (SessionStat) {
  }

  rpc GetPastSessions (PastSessionQuery) returns (PastSessionList);
  rpc GetPastTalks (PastTalkQuery) returns (PastTalkList);
}
