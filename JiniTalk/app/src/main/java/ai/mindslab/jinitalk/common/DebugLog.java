package ai.mindslab.jinitalk.common;

import android.util.Log;

import ai.mindslab.jinitalk.BuildConfig;

public class DebugLog {

	public static final void e(String tag, String msg) {
		if (BuildConfig.DEBUG)
			Log.e(tag, msg);
	}

	public static final void e(String tag, String msg, Throwable tr) {
		if (BuildConfig.DEBUG)
			Log.e(tag, msg, tr);
	}

	public static final void d(String tag, String msg) {
		if (BuildConfig.DEBUG)
			Log.d(tag, msg);
	}

	public static final void i(String tag, String msg) {
		if (BuildConfig.DEBUG)
			Log.i(tag, msg);
	}
}
