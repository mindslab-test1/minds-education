package ai.mindslab.jinitalk.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import ai.mindslab.jinitalk.R;

public class BottomMenu extends LinearLayout implements View.OnClickListener {

	public static final int MENU_TEXT_TALK = 0;
	public static final int MENU_TOUCH_TALK = 1;
	public static final int MENU_VOICE_TALK = 2;
	public static final int MENU_ALARM_TALK = 3;
	public static final int MENU_MEDIA_ZONE = 4;

	private Context context;
	private OnMenuClickListener onMenuClickListener;

	private ImageView textTalkMenu;
	private ImageView touchTalkMenu;
	private ImageView voiceTalkMenu;
	private ImageView alarmTalkMenu;
	private ImageView mediaZoneMenu;

	public BottomMenu(Context context) {
		super(context);
		initialize(context, null);
	}

	public BottomMenu(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize(context, attrs);
	}

	private void initialize(Context context, AttributeSet attrs) {
		this.context = context;

		LayoutInflater.from(context).inflate(R.layout.bottom_menu, this, true);

		textTalkMenu = (ImageView) findViewById(R.id.textTalkMenu);
		touchTalkMenu = (ImageView) findViewById(R.id.touchTalkMenu);
		voiceTalkMenu = (ImageView) findViewById(R.id.voiceTalkMenu);
		alarmTalkMenu = (ImageView) findViewById(R.id.alarmTalkMenu);
		mediaZoneMenu = (ImageView) findViewById(R.id.mediaZoneMenu);

		textTalkMenu.setOnClickListener(this);
		touchTalkMenu.setOnClickListener(this);
		voiceTalkMenu.setOnClickListener(this);
		alarmTalkMenu.setOnClickListener(this);
		mediaZoneMenu.setOnClickListener(this);
	}

	public void setBottomMenu(int menu) {
		textTalkMenu.setEnabled(true);
		touchTalkMenu.setEnabled(true);
		voiceTalkMenu.setEnabled(true);
		alarmTalkMenu.setEnabled(true);
		mediaZoneMenu.setEnabled(true);

		switch (menu) {
			case MENU_TEXT_TALK :
				textTalkMenu.setEnabled(false);
				break;

			case MENU_TOUCH_TALK :
				touchTalkMenu.setEnabled(false);
				break;

			case MENU_VOICE_TALK :
				voiceTalkMenu.setEnabled(false);
				break;

			case MENU_ALARM_TALK :
				alarmTalkMenu.setEnabled(false);
				break;

			case MENU_MEDIA_ZONE :
				mediaZoneMenu.setEnabled(false);
				break;
		}
	}

	public interface OnMenuClickListener {
		void onClick(View v);
	}

	public void setOnMenuClickListener(OnMenuClickListener l) {
		onMenuClickListener = l;
	}

	@Override
	public void onClick(View v) {
		onMenuClickListener.onClick(v);
	}
}
