package ai.mindslab.jinitalk;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import ai.mindslab.jinitalk.common.Constants;
import ai.mindslab.jinitalk.common.Utils;
import ai.mindslab.jinitalk.view.BottomMenu;

public class TouchTalkActivity extends BaseActivity implements View.OnClickListener {

	private GridView gridView;
	private LinearLayout buttonLayout;
	private ImageView editButton;
	private ImageView addButton;
	private ImageView completeButton;
	private LinearLayout messageInputLayout;
	private EditText messageInputField;
	private Button saveMessageButton;

	private GridAdapter gridAdapter;
	private ArrayList<String> messageList = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContainerFrameLayout(R.layout.touch_talk_fragment);
		bottomMenu.setBottomMenu(BottomMenu.MENU_TOUCH_TALK);

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		boolean isInitializedTouchTalk = prefs.getBoolean(Constants.KEY_INITIALIZED_TOUCH_TALK, false);

		if (isInitializedTouchTalk) {
			messageList = Utils.getStringArrayPref(context, Constants.KEY_TOUCH_TALK_LIST);
		} else {
			String[] messageArray = getResources().getStringArray(R.array.touch_talk_default_list);
			messageList = new ArrayList<String>(Arrays.asList(messageArray));

			Utils.setStringArrayPref(context, Constants.KEY_TOUCH_TALK_LIST, messageList);

			SharedPreferences.Editor editor = prefs.edit();
			editor.putBoolean(Constants.KEY_INITIALIZED_TOUCH_TALK, true);
			editor.apply();
		}

		gridView = (GridView) findViewById(R.id.gridView);
		buttonLayout = (LinearLayout) findViewById(R.id.buttonLayout);
		editButton = (ImageView) findViewById(R.id.editButton);
		addButton = (ImageView) findViewById(R.id.addButton);
		completeButton = (ImageView) findViewById(R.id.completeButton);
		messageInputLayout = (LinearLayout) findViewById(R.id.messageInputLayout);
		messageInputField = (EditText) findViewById(R.id.messageInputField);
		saveMessageButton = (Button) findViewById(R.id.saveMessageButton);

		editButton.setOnClickListener(this);
		completeButton.setOnClickListener(this);
		addButton.setOnClickListener(this);
		saveMessageButton.setOnClickListener(this);

		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (gridAdapter.isEditMode() == false) {
					sendMessage(MESSAGE_TYPE_TEXT, messageList.get(position));
				}
			}
		});

		gridAdapter = new GridAdapter(this);
		gridView.setAdapter(gridAdapter);
	}

	@Override
	protected void onDestroy() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(messageInputField.getWindowToken(), 0);
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		if (messageInputLayout.getVisibility() == View.VISIBLE) {
			bottomMenu.setVisibility(View.VISIBLE);
			buttonLayout.setVisibility(View.VISIBLE);
			editButton.setVisibility(View.VISIBLE);
			completeButton.setVisibility(View.GONE);
			messageInputLayout.setVisibility(View.GONE);
			return;
		} else {
			if (completeButton.getVisibility() == View.VISIBLE) {
				editButton.setVisibility(View.VISIBLE);
				completeButton.setVisibility(View.GONE);
				gridAdapter.setEditMode(false);
				return;
			}
		}
		super.onBackPressed();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.editButton:
				editButton.setVisibility(View.GONE);
				completeButton.setVisibility(View.VISIBLE);
				gridAdapter.setEditMode(true);
				break;

			case R.id.completeButton:
				editButton.setVisibility(View.VISIBLE);
				completeButton.setVisibility(View.GONE);
				gridAdapter.setEditMode(false);
				break;

			case R.id.addButton:
				editButton.setVisibility(View.VISIBLE);
				completeButton.setVisibility(View.GONE);
				gridAdapter.setEditMode(false);

				bottomMenu.setVisibility(View.GONE);
				buttonLayout.setVisibility(View.GONE);
				messageInputLayout.setVisibility(View.VISIBLE);

				messageInputField.requestFocus();
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
				break;

			case R.id.saveMessageButton:
				String message = messageInputField.getText().toString().trim();

				if (TextUtils.isEmpty(message)) {
					Toast.makeText(context, "메시지를 입력해주세요.", Toast.LENGTH_SHORT).show();
					messageInputField.setText("");
					return;
				}

				messageList.add(message);
				gridAdapter.notifyDataSetChanged();
				messageInputField.setText("");

				Utils.setStringArrayPref(context, Constants.KEY_TOUCH_TALK_LIST, messageList);
				break;
		}
	}

	/**
	 * 터치톡 목록 Adapter
	 */
	private class GridAdapter extends BaseAdapter {

		private Context context;
		private LayoutInflater inflater;
		private boolean isEditMode = false;

		GridAdapter(Context context) {
			this.context = context;
			inflater = LayoutInflater.from(context);
		}

		void setEditMode(boolean mode) {
			isEditMode = mode;
			notifyDataSetChanged();
		}

		boolean isEditMode() {
			return isEditMode;
		}

		private void removeItem(int position) {
			messageList.remove(position);
			notifyDataSetChanged();
			Utils.setStringArrayPref(context, Constants.KEY_TOUCH_TALK_LIST, messageList);
		}

		@Override
		public int getCount() {
			return messageList.size();
		}

		@Override
		public String getItem(int position) {
			return messageList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			ViewHolder holder;

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.touch_talk_grid_item_view, null);
				holder = new ViewHolder();
				holder.deleteIcon = (ImageView) convertView.findViewById(R.id.deleteIcon);
				holder.messageView = (TextView) convertView.findViewById(R.id.messageView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			if (isEditMode) {
				holder.deleteIcon.setVisibility(View.VISIBLE);
				holder.messageView.setTextColor(0xFF1A1A1A);
				holder.messageView.setBackgroundResource(R.drawable.touch_talk_background_selected);
			} else {
				holder.deleteIcon.setVisibility(View.GONE);
				holder.messageView.setTextColor(0xFF005700);
				holder.messageView.setBackgroundResource(R.drawable.touch_talk_background_normal);
			}

			holder.deleteIcon.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					removeItem(position);
				}
			});

			holder.messageView.setText(getItem(position));

			return convertView;
		}

		private class ViewHolder {
			ImageView deleteIcon;
			TextView messageView;
		}
	}
}
