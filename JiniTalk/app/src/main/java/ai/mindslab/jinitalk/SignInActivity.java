package ai.mindslab.jinitalk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import ai.mindslab.jinitalk.common.Constants;

public class SignInActivity extends Activity {

	private Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signin_activity);

		context = this;

		final EditText idFieldView = (EditText) findViewById(R.id.idTextField);
		final EditText pwFieldView = (EditText) findViewById(R.id.pwTextField);

		findViewById(R.id.signinButton).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String id = idFieldView.getText().toString().trim();
				String pw = pwFieldView.getText().toString().trim();

				if (TextUtils.isEmpty(id)) {
					Toast.makeText(context, "아이디를 입력해주세요", Toast.LENGTH_SHORT).show();
					return;
				}

				if (TextUtils.isEmpty(pw)) {
					Toast.makeText(context, "비밀번호를 입력해주세요", Toast.LENGTH_SHORT).show();
					return;
				}

				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString(Constants.KEY_USER_ID, id);
				editor.apply();

				Intent intent = new Intent(SignInActivity.this, TextTalkActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);

				finish();
			}
		});
	}
}
