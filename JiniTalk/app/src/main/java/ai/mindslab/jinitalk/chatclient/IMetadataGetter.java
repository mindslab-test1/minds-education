package ai.mindslab.jinitalk.chatclient;

import io.grpc.Metadata;

public interface IMetadataGetter {

    void onMetaData(Metadata md);
}
