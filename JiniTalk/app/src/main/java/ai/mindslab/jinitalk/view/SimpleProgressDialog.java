package ai.mindslab.jinitalk.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import ai.mindslab.jinitalk.R;

public class SimpleProgressDialog extends ProgressDialog {

	public SimpleProgressDialog(Context context) {
		super(context);
		super.setCancelable(false);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.simple_progress_bar);
	}

	public void setCancelable(boolean flag) {
		super.setCancelable(flag);
	}
}
