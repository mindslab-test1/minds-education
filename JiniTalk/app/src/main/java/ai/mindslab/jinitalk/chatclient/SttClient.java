package ai.mindslab.jinitalk.chatclient;

import android.content.Context;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import elsa.facade.Dialog;
import elsa.facade.SpeechToTextServiceGrpc;
import elsa.facade.SpeechToTextServiceGrpc.SpeechToTextServiceStub;
import io.grpc.Channel;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.okhttp.OkHttpChannelBuilder;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;

public class SttClient {
  private final String TAG = SttClient.class.getSimpleName();

  private Context context;
  private SpeechToTextServiceStub speechToTextServiceStub;
  private ManagedChannel originalChannel;

  Long sessionId_;
  public static final String GRPC_HOST_ADDRESS = "125.132.250.204";
  public static final Integer GRPC_PORT = 9901;
  private static final Integer audioSampleRate = 16000;

  public enum ChatLanguage {
    kor,
    eng
  }

  private ChatLanguage chatLanguage = ChatLanguage.kor;

  /**
   * Constructor
   */
  public SttClient(Context context, IMetadataGetter mg) {
    final Channel underlyingChannel;

    this.context = context;
    originalChannel = OkHttpChannelBuilder.forAddress(GRPC_HOST_ADDRESS,
        GRPC_PORT).usePlaintext(true).build();

    HeaderClientInterceptor headerClientInterceptor = new HeaderClientInterceptor();
    headerClientInterceptor.setMetadataGetter(mg);

    underlyingChannel = ClientInterceptors.intercept(originalChannel, headerClientInterceptor);
    speechToTextServiceStub = SpeechToTextServiceGrpc.newStub(underlyingChannel);
  }

  /**
   * Grpc shutdown.
   */
  public void shutdown() throws InterruptedException {
    originalChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
  }

  /**
   * Speech to text.
   */
  public StreamObserver<Dialog.AudioUtter> getSenderStt(final ChatLanguage lang, final IAudioTextCallback atc) {
    /* Set meta data */
    Metadata.Key<String> provider =
        Metadata.Key.of("x-elsa-authentication-provider", Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> audioHz =
        Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> language =
        Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
    Metadata header = new Metadata();
    header.put(provider, "mindslab");
    header.put(audioHz, "" + audioSampleRate);
    header.put(language, lang.name());

    speechToTextServiceStub = MetadataUtils.attachHeaders(speechToTextServiceStub, header);
    Log.d(TAG, "[GRPC Header]" + header.toString());

    /* Request getServiceGroup */
    return speechToTextServiceStub.simpleRecognize(new StreamObserver<Dialog.TextUtter>() {
      @Override
      public void onNext(Dialog.TextUtter value) {
        System.out.println("simpleRecognize onNext() : " + value.getUtter());
        atc.onText(value.getUtter());
      }

      @Override
      public void onError(Throwable t) {
        System.out.println("simpleRecognize onError() : " + t.toString());
        atc.onError(t);
      }

      @Override
      public void onCompleted() {
        System.out.println("simpleRecognize onCompleted()");
      }
    });
  }


  /**
   * Get chatLanguage.
   */
  public ChatLanguage getChatLanguage() {
    return chatLanguage;
  }

  /**
   * Set chatLanguage.
   */
  public void setChatLanguage(ChatLanguage chatLanguage) {
    this.chatLanguage = chatLanguage;
  }
}


