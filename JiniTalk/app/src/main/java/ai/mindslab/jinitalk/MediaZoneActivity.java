package ai.mindslab.jinitalk;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ai.mindslab.jinitalk.view.BottomMenu;

public class MediaZoneActivity extends BaseActivity {

	private static final int VIDEO_TAB_START = 0;
	private static final int VIDEO_TAB_END = 3;
	private static final int AUDIO_TAB_START = 4;
	private static final int AUDIO_TAB_END = 8;

	private ImageView videoTab;
	private ImageView audioTab;
	private LinearLayout subTabLayout;
	private ListView listView;
	private MediaListAdapter listAdapter;

	private ArrayList<String[]> mediaList = new ArrayList<String[]>();
	private String[] subTabMenuList;
	private int selectedTabIndex = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContainerFrameLayout(R.layout.media_zone_fragment);
		bottomMenu.setBottomMenu(BottomMenu.MENU_MEDIA_ZONE);

		videoTab = (ImageView) findViewById(R.id.videoTab);
		audioTab = (ImageView) findViewById(R.id.audioTab);
		subTabLayout = (LinearLayout) findViewById(R.id.subTabLayout);
		listView = (ListView) findViewById(R.id.listView);

		// TODO : Text code
		mediaList.add(getResources().getStringArray(R.array.video_list_1));
		mediaList.add(getResources().getStringArray(R.array.video_list_2));
		mediaList.add(getResources().getStringArray(R.array.video_list_3));
		mediaList.add(getResources().getStringArray(R.array.video_list_4));
		mediaList.add(getResources().getStringArray(R.array.audio_list_1));
		mediaList.add(getResources().getStringArray(R.array.audio_list_2));
		mediaList.add(getResources().getStringArray(R.array.audio_list_3));
		mediaList.add(getResources().getStringArray(R.array.audio_list_4));
		mediaList.add(getResources().getStringArray(R.array.audio_list_5));

		videoTab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selectedTabIndex = VIDEO_TAB_START;
				setTabLayout();
			}
		});

		audioTab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selectedTabIndex = AUDIO_TAB_START;
				setTabLayout();
			}
		});

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				try {
					String fileName = mediaList.get(selectedTabIndex)[position];
					fileName = fileName.substring(0, 2) + fileName.substring(fileName.lastIndexOf("."));
					String filePath = "file:///storage/sdcard1/Contents/" + (selectedTabIndex + 1) + "/" + fileName;

					if (selectedTabIndex <= VIDEO_TAB_END) {
						sendMessage(MESSAGE_TYPE_VIDEO, filePath);
					} else {
						sendMessage(MESSAGE_TYPE_AUDIO, filePath);
					}
				} catch (IndexOutOfBoundsException e) {
					e.printStackTrace();
				}
			}
		});

		videoTab.setEnabled(false);

		subTabMenuList = getResources().getStringArray(R.array.media_zone_sub_tab_list);
		selectedTabIndex = VIDEO_TAB_START;
		setTabLayout();
	}

	/**
	 * 미디어존 서브탭 메뉴 레이아웃 설정
	 */
	private void setTabLayout() {
		subTabLayout.removeAllViews();

		int startTabIndex;
		int endTabIndex;

		if (selectedTabIndex <= VIDEO_TAB_END) {
			startTabIndex = VIDEO_TAB_START;
			endTabIndex = VIDEO_TAB_END;
			videoTab.setEnabled(false);
			audioTab.setEnabled(true);
		} else {
			startTabIndex = AUDIO_TAB_START;
			endTabIndex = AUDIO_TAB_END;
			videoTab.setEnabled(true);
			audioTab.setEnabled(false);
		}

		if (subTabMenuList != null) {
			for (int i=startTabIndex; i<=endTabIndex; i++) {
				View convertView = LayoutInflater.from(this).inflate(R.layout.medial_zone_sub_tab, null);
				ImageView divider = (ImageView) convertView.findViewById(R.id.divider);
				TextView title = (TextView) convertView.findViewById(R.id.title);
				title.setText(subTabMenuList[i]);

				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, 1);
				convertView.setLayoutParams(params);

				if (i == selectedTabIndex) {
					convertView.setEnabled(false);
				} else {
					convertView.setEnabled(true);
				}

				if (i == startTabIndex || i == selectedTabIndex || i == (selectedTabIndex + 1)) {
					divider.setVisibility(View.INVISIBLE);
				}

				final int index = i;

				convertView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						selectedTabIndex = index;
						setTabLayout();
					}
				});

				subTabLayout.addView(convertView);
			}
		}

		listAdapter = new MediaListAdapter();
		listView.setAdapter(listAdapter);
	}

	/**
	 * 미디어 목록 Adapter
	 */
	private class MediaListAdapter extends BaseAdapter {

		MediaListAdapter() {
			super();
		}

		@Override
		public int getCount() {
			return mediaList.get(selectedTabIndex).length;
		}

		@Override
		public String getItem(int position) {
			return mediaList.get(selectedTabIndex)[position];
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;

			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(R.layout.media_zone_list_item, null);
				holder = new ViewHolder();
				holder.thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);
				holder.title = (TextView) convertView.findViewById(R.id.title);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.thumbnail.setImageResource(R.drawable.img_media_zone_dummy);
			holder.title.setText(getItem(position));

			return convertView;
		}

		class ViewHolder {
			ImageView thumbnail;
			TextView title;
		}
	}
}
