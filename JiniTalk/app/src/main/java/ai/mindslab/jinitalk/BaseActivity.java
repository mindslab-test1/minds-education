package ai.mindslab.jinitalk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import ai.mindslab.jinitalk.common.ApiHttpHeader;
import ai.mindslab.jinitalk.common.Constants;
import ai.mindslab.jinitalk.common.DebugLog;
import ai.mindslab.jinitalk.view.BottomMenu;
import ai.mindslab.jinitalk.view.SimpleProgressDialog;

public class BaseActivity extends Activity {

	protected static final int MESSAGE_TYPE_TEXT = 1;
	protected static final int MESSAGE_TYPE_VIDEO = 2;
	protected static final int MESSAGE_TYPE_AUDIO = 3;

	protected ImageView titleImageView;
	protected ImageView drawerMenuButton;

	protected FrameLayout containerFrameLayout;
	protected ScrollView drawerMenuScrollView;
	protected DrawerLayout drawerLayout;

	protected BottomMenu bottomMenu;
	protected SimpleProgressDialog progressDialog;

	protected Context context;
	protected RequestQueue requestQueue; // Request queue
	protected String userId;

	private boolean isBackKeyPressed = false;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.base_drawer_activity);

		context = this;

		drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
		containerFrameLayout = (FrameLayout) findViewById(R.id.containerFrameLayout);
		drawerMenuScrollView = (ScrollView) findViewById(R.id.drawerMenuScrollView);

		drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {

			}

			@Override
			public void onDrawerOpened(View drawerView) {

			}

			@Override
			public void onDrawerClosed(View drawerView) {

			}

			@Override
			public void onDrawerStateChanged(int newState) {

			}
		});
		drawerLayout.setScrimColor(0x45000000);

		progressDialog = new SimpleProgressDialog(context);

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		userId = prefs.getString(Constants.KEY_USER_ID, "");

		// Initialize Volley
		requestQueue = Volley.newRequestQueue(this);
		requestQueue.getCache().clear();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		requestQueue.cancelAll("SEND");
		requestQueue.getCache().clear();
		requestQueue.stop();
	}

	@Override
	public void onBackPressed() {
		if (isBackKeyPressed == false) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					isBackKeyPressed = false;
				}
			}, 3000);

			isBackKeyPressed = true;
			Toast.makeText(this, "한번더 뒤로가기를 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
			return;
		}
		super.onBackPressed();
	}

	/**
	 * 화면 레이아웃 설정
	 * @param resource 리소스 ID
	 */
	protected void setContainerFrameLayout(int resource) {
		View view = LayoutInflater.from(this).inflate(resource, null);
		containerFrameLayout.addView(view);

		titleImageView = (ImageView) findViewById(R.id.titleImageView);
		drawerMenuButton = (ImageView) findViewById(R.id.drawerMenuButton);

		if (drawerMenuButton != null) {
			drawerMenuButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					drawerLayout.openDrawer(drawerMenuScrollView);
				}
			});
		}

		bottomMenu = (BottomMenu) findViewById(R.id.bottomMenu);

		if (bottomMenu != null) {
			bottomMenu.setOnMenuClickListener(new BottomMenu.OnMenuClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = null;

					switch (v.getId()) {
						case R.id.textTalkMenu:
							intent = new Intent(BaseActivity.this, TextTalkActivity.class);
							break;

						case R.id.touchTalkMenu:
							intent = new Intent(BaseActivity.this, TouchTalkActivity.class);
							break;

						case R.id.voiceTalkMenu:
							intent = new Intent(BaseActivity.this, VoiceTalkActivity.class);
							break;

						case R.id.alarmTalkMenu:
							intent = new Intent(BaseActivity.this, AlarmTalkActivity.class);
							break;

						case R.id.mediaZoneMenu:
							intent = new Intent(BaseActivity.this, MediaZoneActivity.class);
							break;
					}

					if (intent != null) {
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.hold, R.anim.hold);

						finish();
					}
				}
			});
		}
	}

	/**
	 * NeoIDM 메시지 보내기
	 * @param type 메시지 타입 (1: 텍스트, 2: 비디오, 3: 오디오)
	 * @param message 메시지 내용
	 */
	protected void sendMessage(int type, String message) {
		progressDialog.show();

		String apiUrl = "http://219.251.4.153:8080/v1.0/clients/MindsLabEduDemo5/3341/0/5527?at=936d1419-afcd-46ff-979b-f0d93f17414c";

		JSONObject valueObj = new JSONObject();
		JSONObject requestObj = new JSONObject();

		try {
			valueObj.put("type", type);
			valueObj.put("message", message);

			requestObj.put("id", 5527);
			requestObj.put("value", valueObj.toString());
		} catch (JSONException e) {
			return;
		}
		DebugLog.i("JiniTalk", requestObj.toString());

		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.PUT, apiUrl, requestObj, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject responseObj) {
				try {
					DebugLog.i("JiniTalk", responseObj.toString());
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (progressDialog.isShowing()) {
						progressDialog.dismiss();
					}
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError e) {
				DebugLog.e("JiniTalk", "VolleyError", e);

				if (progressDialog.isShowing()) {
					progressDialog.dismiss();
				}
				Toast.makeText(context, "인터넷 연결 상태를 확인해주세요", Toast.LENGTH_SHORT).show();
			}
		}) {
			@Override
			protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
				Map<String, String> responseHeaders = response.headers;
				for (String s : responseHeaders.keySet()) {
					DebugLog.d("JiniTalk", s + " : " + responseHeaders.get(s));
				}
				return super.parseNetworkResponse(response);
			}

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> header = super.getHeaders();
				Map<String, String> params = ApiHttpHeader.setHTTPHeader("");
				params.putAll(header);
				return params;
			}
		};

		jsObjRequest.setTag("SEND");
		jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(1000 * 10, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		requestQueue.add(jsObjRequest);
	}
}
