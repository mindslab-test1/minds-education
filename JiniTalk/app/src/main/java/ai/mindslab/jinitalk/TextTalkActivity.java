package ai.mindslab.jinitalk;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ai.mindslab.jinitalk.common.Constants;
import ai.mindslab.jinitalk.common.Utils;
import ai.mindslab.jinitalk.view.BottomMenu;

public class TextTalkActivity extends BaseActivity {

	private ListView listView;
	private LinearLayout buttonLayout;
	private ImageView composeMessageButton;
	private LinearLayout messageInputLayout;
	private EditText messageInputField;
	private Button sendMessageButton;

	private MessageListAdapter listAdapter;
	private ArrayList<String> messageList = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContainerFrameLayout(R.layout.text_talk_fragment);
		bottomMenu.setBottomMenu(BottomMenu.MENU_TEXT_TALK);

		messageList = Utils.getStringArrayPref(context, Constants.KEY_TEXT_TALK_LIST);

		listView = (ListView) findViewById(R.id.listView);
		buttonLayout = (LinearLayout) findViewById(R.id.buttonLayout);
		composeMessageButton = (ImageView) findViewById(R.id.composeMessageButton);
		messageInputLayout = (LinearLayout) findViewById(R.id.messageInputLayout);
		messageInputField = (EditText) findViewById(R.id.messageInputField);
		sendMessageButton = (Button) findViewById(R.id.sendMessageButton);

		composeMessageButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bottomMenu.setVisibility(View.GONE);
				buttonLayout.setVisibility(View.GONE);
				messageInputLayout.setVisibility(View.VISIBLE);

				messageInputField.requestFocus();
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
			}
		});

		sendMessageButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String message = messageInputField.getText().toString().trim();

				if (TextUtils.isEmpty(message)) {
					Toast.makeText(context, "메시지를 입력해주세요.", Toast.LENGTH_SHORT).show();
					messageInputField.setText("");
					return;
				}

				sendMessage(MESSAGE_TYPE_TEXT, message);

				messageList.add(message);
				listAdapter.notifyDataSetChanged();
				messageInputField.setText("");

				Utils.setStringArrayPref(context, Constants.KEY_TEXT_TALK_LIST, messageList);
			}
		});

		listAdapter = new MessageListAdapter(this);
		listView.setAdapter(listAdapter);
	}

	@Override
	protected void onDestroy() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(messageInputField.getWindowToken(), 0);
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		if (messageInputLayout.getVisibility() == View.VISIBLE) {
			bottomMenu.setVisibility(View.VISIBLE);
			buttonLayout.setVisibility(View.VISIBLE);
			messageInputLayout.setVisibility(View.GONE);
			return;
		}
		super.onBackPressed();
	}

	/**
	 * 텍스트톡 목록 Adapter
	 */
	private class MessageListAdapter extends BaseAdapter {

		private LayoutInflater inflater;

		MessageListAdapter(Context context) {
			inflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			return messageList.size();
		}

		@Override
		public String getItem(int position) {
			return messageList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.text_talk_list_item_view, null);
				holder = new ViewHolder();
				holder.messageView = (TextView) convertView.findViewById(R.id.messageView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.messageView.setText(getItem(position));

			return convertView;
		}

		class ViewHolder {
			TextView messageView;
		}
	}
}
