package ai.mindslab.jinitalk.chatclient;

public interface IAudioTextCallback {
    void onText(String text);
    void onError(Throwable t);
}
