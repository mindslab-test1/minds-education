package ai.mindslab.jinitalk.common;

import java.util.HashMap;
import java.util.Map;

public class ApiHttpHeader {

	public static Map<String, String> setHTTPHeader(String accessToken) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("Content-type", "application/json; charset=UTF-8");
		params.put("Accept", "application/json");
		params.put("x-elsa-authentication-provider", "mindslab");
		params.put("x-elsa-authentication-token", accessToken);

		return params;
	}
}
