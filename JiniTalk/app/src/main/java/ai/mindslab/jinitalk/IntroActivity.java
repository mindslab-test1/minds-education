package ai.mindslab.jinitalk;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

public class IntroActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.intro_activity);

		final ImageView introImageView = (ImageView) findViewById(R.id.introImageView);
		introImageView.setImageResource(R.drawable.intro_animation);
		AnimationDrawable frameAnimation = (AnimationDrawable) introImageView.getDrawable();
		frameAnimation.start();

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(IntroActivity.this, SignInActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);

				finish();
			}
		}, 3000);
	}

	@Override
	public void onBackPressed() {

	}
}
