package ai.mindslab.jinitalk;

import android.os.Bundle;

import ai.mindslab.jinitalk.view.BottomMenu;

public class AlarmTalkActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContainerFrameLayout(R.layout.alarm_talk_fragment);
		bottomMenu.setBottomMenu(BottomMenu.MENU_ALARM_TALK);
	}
}
