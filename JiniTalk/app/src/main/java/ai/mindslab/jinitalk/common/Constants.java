package ai.mindslab.jinitalk.common;

public class Constants {

	// API
	public static final String NEOIDM_API_SERVER = "http://219.251.4.153:8080";
	public static final String NEOIDM_API_ACCESS_TOKEN = "2292969e-d655-4ecb-9e24-887a8963a705";
	public static final String NEOIDM_API_SEND_MESSAGE = "/v1.0/clients/%s?at=%s";

	// SharedPreferences
	public static final String KEY_USER_ID = "UserId";
	public static final String KEY_TEXT_TALK_LIST = "TextTalkList";
	public static final String KEY_INITIALIZED_TOUCH_TALK = "InitializedTouchTalk";
	public static final String KEY_TOUCH_TALK_LIST = "TouchTalkList";

	// Intent extra name
	public static final String ALARM_TALK_MODE_NAME = "AlarmTalkMode";

	// Alarm Talk
	public static final int ALARM_GETUP = 1;
	public static final int ALARM_SLEEP = 2;
	public static final int ALARM_BRUSH_TEETH = 3;
	public static final int ALARM_BATH = 4;
	public static final int ALARM_DRESS = 5;
	public static final int ALARM_BIRTHDAY = 6;

}
