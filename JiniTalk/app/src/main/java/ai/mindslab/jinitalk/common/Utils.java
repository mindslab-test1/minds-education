package ai.mindslab.jinitalk.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;

public class Utils {

	public static void setStringArrayPref(Context context, String key, ArrayList<String> list) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = prefs.edit();
		JSONArray jsonArray = new JSONArray();

		for (int i = 0; i < list.size(); i++) {
			jsonArray.put(list.get(i));
		}

		if (!list.isEmpty()) {
			editor.putString(key, jsonArray.toString());
		} else {
			editor.putString(key, null);
		}

		editor.apply();
	}

	public static ArrayList<String> getStringArrayPref(Context context, String key) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String jsonString = prefs.getString(key, null);
		ArrayList<String> list = new ArrayList<String>();

		if (TextUtils.isEmpty(jsonString) == false) {
			try {
				JSONArray jsonArray = new JSONArray(jsonString);
				for (int i = 0; i < jsonArray.length(); i++) {
					String str = jsonArray.optString(i);
					list.add(str);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return list;
	}

	public static String getExternalStorageDirectory() {
		String path = null;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			File primaryExternalStorage = Environment.getExternalStorageDirectory();
			String externalStorageRootDir = primaryExternalStorage.getParent();

			if (externalStorageRootDir == null) {
				path = primaryExternalStorage.getAbsolutePath();
			} else {
				File externalStorageRoot = new File("/storage");
				File[] files = externalStorageRoot.listFiles();

				if (files != null) {
					for (File file : files) {
						if (file.isDirectory() && file.canRead() && (file.listFiles().length > 0)) {
							path = file.getAbsolutePath();
						}
					}
				}
			}
		}

		return path;
	}
}
