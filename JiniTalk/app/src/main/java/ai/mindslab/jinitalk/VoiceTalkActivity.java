package ai.mindslab.jinitalk;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Toast;

import com.google.protobuf.ByteString;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Timer;
import java.util.TimerTask;

import ai.mindslab.jinitalk.chatclient.IAudioTextCallback;
import ai.mindslab.jinitalk.chatclient.IMetadataGetter;
import ai.mindslab.jinitalk.chatclient.SttClient;
import ai.mindslab.jinitalk.common.DebugLog;
import ai.mindslab.jinitalk.view.BottomMenu;
import elsa.facade.Dialog;
import io.grpc.Metadata;
import io.grpc.stub.StreamObserver;

public class VoiceTalkActivity extends BaseActivity implements View.OnClickListener {

	private static final String TAG = VoiceTalkActivity.class.getSimpleName();

	private static final int RECORDER_SAMPLERATE = 16000;
	private static final int RECORDER_CHANNEL_CONFIG = AudioFormat.CHANNEL_IN_MONO;
	private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
	private static final String RECORDING_FILE_NAME = "VoiceTalk.wav";

	private ProgressBar recorderProgressBar;
	private ImageView recorderCarImage;
	private ImageView recorderStartButton;
	private ImageView recorderStopButton;
	private LinearLayout mediaPlayerLayout;
	private ImageView mediaPlayButton;
	private ImageView mediaPauseButton;
	private SeekBar playerSeekBar;
	private ImageView sendButton;

	private AudioRecord audioRecorder;
	private AudioTrack audioTrack;
	private Thread recordingThread;

	private int minBuffSizeInByte;
	private int minBuffSizeInShort;
	private boolean isRecording = false;
	private boolean isPlaying = false;

	private short readData[] = null;
	private SttClient sttClient;
	private StreamObserver<Dialog.AudioUtter> sender;
	private String outputFile;
	private String resultVoiceTalk = "";

	private RecorderCarAnimationHandler recorderCarAnimationHandler;
	private float rotateAngle = 0;
	private Timer recorderCarAnimationTimer = new Timer();
	private Timer recorderProgressBarTimer = new Timer();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContainerFrameLayout(R.layout.voice_talk_fragment);
		bottomMenu.setBottomMenu(BottomMenu.MENU_VOICE_TALK);

		recorderProgressBar = (ProgressBar) findViewById(R.id.recorderProgressBar);
		recorderCarImage = (ImageView) findViewById(R.id.recorderCarImage);
		recorderStartButton = (ImageView) findViewById(R.id.recorderStartButton);
		recorderStopButton = (ImageView) findViewById(R.id.recorderStopButton);
		mediaPlayerLayout = (LinearLayout) findViewById(R.id.mediaPlayerLayout);
		mediaPlayButton = (ImageView) findViewById(R.id.mediaPlayButton);
		mediaPauseButton = (ImageView) findViewById(R.id.mediaPauseButton);
		playerSeekBar = (SeekBar) findViewById(R.id.playerSeekBar);
		sendButton = (ImageView) findViewById(R.id.sendButton);

		mediaPlayerLayout.setSelected(false);
		playerSeekBar.setProgress(0);
		playerSeekBar.setEnabled(false);
		sendButton.setEnabled(false);

		recorderStartButton.setOnClickListener(this);
		recorderStopButton.setOnClickListener(this);
		mediaPlayButton.setOnClickListener(this);
		mediaPauseButton.setOnClickListener(this);
		sendButton.setOnClickListener(this);

		recorderCarAnimationHandler = new RecorderCarAnimationHandler(this);

		outputFile = this.getFileStreamPath(RECORDING_FILE_NAME).getAbsolutePath();

		sttClient = new SttClient(getBaseContext(), new IMetadataGetter() {
			@Override
			public void onMetaData(Metadata md) {
				DebugLog.i(TAG, "[STT] " + md.toString());
			}
		});
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (isRecording) {
			stopRecording();
		}

		if (isPlaying) {
			isPlaying = false;
		}

		if (progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		try {
			sttClient.shutdown();
			deleteRecordingFile();
		} catch (InterruptedException e) {
			DebugLog.e(TAG, "InterruptedException", e);
		} catch (Exception e) {
			DebugLog.e(TAG, "Exception", e);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.recorderStartButton:
				startRecording();
				break;

			case R.id.recorderStopButton:
				stopRecording();
				break;

			case R.id.mediaPlayButton:
				File file = new File(outputFile);

				if (file.exists()) {
					new PlaybackTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				} else {
					Toast.makeText(context, "녹음 내용이 없습니다.", Toast.LENGTH_SHORT).show();
				}
				break;

			case R.id.mediaPauseButton:
				isPlaying = false;
				break;

			case R.id.sendButton:
				if (TextUtils.isEmpty(resultVoiceTalk) == false) {
					sendMessage(MESSAGE_TYPE_TEXT, resultVoiceTalk);
				}
				break;
		}
	}

	/**
	 * 녹음 시작
	 */
	private void startRecording() {
		DebugLog.d(TAG, "Recording Start");

		if (isRecording == true || recordingThread != null) {
			return;
		}

		// Recorder Button
		recorderStartButton.setVisibility(View.GONE);
		recorderStopButton.setVisibility(View.VISIBLE);
		mediaPlayerLayout.setSelected(false);
		sendButton.setEnabled(false);

		// Circular ProgressBar
		recorderProgressBar.setProgress(0);
		recorderProgressBarTimer = new Timer();
		recorderProgressBarTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				if (recorderProgressBar.getProgress() < recorderProgressBar.getMax()) {
					recorderProgressBar.setProgress(recorderProgressBar.getProgress() + 1);
				} else {
					recorderProgressBarTimer.cancel();
				}
			}
		}, 50, 50);

		// Car animation
		rotateAngle = 0;
		recorderCarImage.clearAnimation();
		recorderCarAnimationTimer = new Timer();
		recorderCarAnimationTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				rotateAngle += 0.6;
				recorderCarAnimationHandler.sendMessage(recorderCarAnimationHandler.obtainMessage(0, rotateAngle));
			}
		}, 50, 50);

		// STT
		sender = sttClient.getSenderStt(SttClient.ChatLanguage.kor, new IAudioTextCallback() {
			@Override
			public void onText(String text) {
				DebugLog.i(TAG, "[STT Response text] " + text);

				resultVoiceTalk = text;

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (TextUtils.isEmpty(resultVoiceTalk) == false) {
							mediaPlayerLayout.setSelected(true);
							sendButton.setEnabled(true);
						} else {
							deleteRecordingFile();
						}

						if (progressDialog.isShowing()) {
							progressDialog.dismiss();
						}
					}
				});
			}

			@Override
			public void onError(final Throwable t) {
				DebugLog.e(TAG, "[STT Response error] " + t.toString());

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						stopRecording();
						deleteRecordingFile();

						if (progressDialog.isShowing()) {
							progressDialog.dismiss();
						}

						Toast.makeText(context, "[STT Response error] " + t.toString(), Toast.LENGTH_SHORT).show();
					}
				});
			}
		});

		// Recording ready
		minBuffSizeInByte = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNEL_CONFIG, RECORDER_AUDIO_ENCODING);

		if (minBuffSizeInByte < 8000) {
			minBuffSizeInByte = 8000;
		}

		minBuffSizeInShort = minBuffSizeInByte / 2;
		audioRecorder = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, RECORDER_SAMPLERATE, RECORDER_CHANNEL_CONFIG, RECORDER_AUDIO_ENCODING, minBuffSizeInByte * 4);
		audioRecorder.startRecording();

		readData = new short[minBuffSizeInShort];
		isRecording = true;
		resultVoiceTalk = "";

		recordingThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					recordLoop();
				} catch (Exception e) {
					DebugLog.e(TAG, "Exception", e);
				}
			}
		}, "AudioRecord Thread");

		recordingThread.start();
	}

	/**
	 * 녹음파일 삭제
	 */
	private void deleteRecordingFile() {
		File file = new File(outputFile);

		if (file.exists()) {
			if (file.delete()) {
				DebugLog.i(TAG, "Recording file deleted");
			}
		}
	}

	/**
	 * 녹음 진행 루프
	 */
	private void recordLoop() {
		DebugLog.d(TAG, "Recording...");

		try {
			short max1 = 0;

			FileOutputStream fileOutputStream = new FileOutputStream(outputFile);

			while (isRecording) {
				short max2 = 0;

				// should read short data when we use PCM 16
				int read = audioRecorder.read(readData, 0, minBuffSizeInShort);

				if (AudioRecord.ERROR_INVALID_OPERATION == read) {
					break;
				}

				ByteBuffer byteBuffer = ByteBuffer.allocate(minBuffSizeInByte);
				byteBuffer.order(ByteOrder.LITTLE_ENDIAN);

				for (int i = 0; i < minBuffSizeInShort; i++) {
					byteBuffer.putShort(readData[i]);

					if (max1 < readData[i]) {
						max1 = readData[i];
					}
					if (max2 < readData[i]) {
						max2 = readData[i];
					}
				}

				byte[] b = byteBuffer.array();

				for (int i = 0; i < 25; i++) {
					System.arraycopy(readData, i * 160, new short[160], 0, 160);
				}

				Dialog.AudioUtter.Builder builder = Dialog.AudioUtter.newBuilder();
				ByteString byteString = ByteString.copyFrom(b);
				builder.setUtter(byteString);
				Dialog.AudioUtter utter = builder.build();
				sender.onNext(utter);

				fileOutputStream.write(b, 0, minBuffSizeInShort * 2);
			}

			sender.onCompleted();

			audioRecorder.stop();
			audioRecorder.release();
			audioRecorder = null;
			recordingThread = null;

			fileOutputStream.close();

		} catch (FileNotFoundException e) {
			DebugLog.e(TAG, "FileNotFoundException", e);
		} catch (IOException e) {
			DebugLog.e(TAG, "IOException", e);
		}
	}

	/**
	 * 녹음 중지
	 */
	private void stopRecording() {
		DebugLog.d(TAG, "Recording Stop");

		isRecording = false;

		progressDialog.show();

		recorderProgressBarTimer.cancel();
		recorderCarAnimationTimer.cancel();

		recorderStartButton.setVisibility(View.VISIBLE);
		recorderStopButton.setVisibility(View.GONE);
	}

	/**
	 * 녹음 중 자동차 애니메이션 표시
	 * @param msg Handle system messages
	 */
	private void recorderCarAnimation(Message msg) {
		float angle = (Float) msg.obj;

		if (angle < 360) {
			RotateAnimation anim = new RotateAnimation(angle, angle + 0.6f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			anim.setInterpolator(new LinearInterpolator());
			anim.setDuration((long) 250);
			anim.setRepeatCount(0);
			anim.setFillAfter(true);
			anim.setFillEnabled(true);
			recorderCarImage.startAnimation(anim);
		} else {
			stopRecording();
			recorderCarAnimationTimer.cancel();
		}
	}

	/**
	 * 녹음 중 자동차 애니메이션 표시 Handler
	 */
	private static class RecorderCarAnimationHandler extends Handler {
		private final WeakReference<VoiceTalkActivity> voiceTalkActivity;

		RecorderCarAnimationHandler(VoiceTalkActivity activity) {
			this.voiceTalkActivity = new WeakReference<VoiceTalkActivity>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			if (voiceTalkActivity.get() != null) {
				voiceTalkActivity.get().recorderCarAnimation(msg);
			}
		}
	}

	/**
	 * 녹음파일 재생 AsyncTask
	 */
	private class PlaybackTask extends AsyncTask<String, Integer, Boolean> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// Volume 50%
//			AudioManager audioManager = (AudioManager)getSystemService(context.AUDIO_SERVICE);
//			audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int)(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * 0.5), AudioManager.FLAG_PLAY_SOUND);

			mediaPlayButton.setVisibility(View.GONE);
			mediaPauseButton.setVisibility(View.VISIBLE);

			minBuffSizeInByte = AudioTrack.getMinBufferSize(RECORDER_SAMPLERATE, AudioFormat.CHANNEL_OUT_MONO, RECORDER_AUDIO_ENCODING);

			if (minBuffSizeInByte < 8000) {
				minBuffSizeInByte = 8000;
			}

			audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, RECORDER_SAMPLERATE, AudioFormat.CHANNEL_OUT_MONO, RECORDER_AUDIO_ENCODING, minBuffSizeInByte * 4, AudioTrack.MODE_STREAM);
			audioTrack.setStereoVolume(1.0f, 1.0f);
			audioTrack.play();

			isPlaying = true;
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				File file = new File(outputFile);
				FileInputStream fileInputStream = new FileInputStream(file);

				byte[] byteData = new byte[minBuffSizeInByte * 4];
				int size = (int) file.length();
				int totalRead = 0;

				while (isPlaying && totalRead < size) {
					int read = fileInputStream.read(byteData, 0, minBuffSizeInByte * 4);

					if (read != -1) {
						audioTrack.write(byteData, 0, read);
						totalRead += read;

						publishProgress(totalRead, size);
					} else {
						break;
					}
				}

				audioTrack.stop();
				audioTrack.release();
				audioTrack = null;

				fileInputStream.close();

			} catch (FileNotFoundException e) {
				DebugLog.e(TAG, "FileNotFoundException", e);
				return false;
			} catch (IOException e) {
				DebugLog.e(TAG, "IOException", e);
				return false;
			}

			return true;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values[0]);

			float value = (float)values[0] / (float)values[1] * 100;
			playerSeekBar.setProgress((int)value);
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			isPlaying = false;

			mediaPlayButton.setVisibility(View.VISIBLE);
			mediaPauseButton.setVisibility(View.GONE);
		}
	}
}
