package ai.mindslab.stoy.view;

/*
 * Created by hybell on 2016. 12. 27..
 */
import static ai.mindslab.stoy.media.MusicPlayer.STOP_STATE;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import ai.mindslab.stoy.R;
import ai.mindslab.stoy.media.MusicPlayer;

/**
 * Classes for ChatAudioContent container.
 */
public class ChatAudioContent extends ChatMetaObject {
  private Context context;
  private String audioUrl;
  private String mimeType;
  private String sampleRate;

  private TextView utterMessageView;
  private ImageButton skipPrev;
  private ImageButton skipNext;
  private ImageButton playPause;
  private TextView startTime;
  private TextView endTime;
  private SeekBar playSeekBar;
  private TextView musicTitleLineFirst;
  private TextView musicTitleLineSecond;
  private TextView musicTitleLineThird;
  private ProgressBar loadingProgressBar;
  private View controllersView;
  private Drawable pauseDrawable;
  private Drawable playDrawable;
  private ImageView backgroundImage;

  // Music player
  private MusicPlayer musicPlayer;
  // 재생할 목록
  private ArrayList<String> playList = null;
  // 마지막 재생 index 를 저장한다.
  private int currentPlayIndex = -1;

  // Play state
  private int state = STOP_STATE;//0 : stop, 1:playing.


  public ChatAudioContent(Context context, ArrayList<String> playList, String mimeType, String sampleRate) {
    this.context = context;
    this.playList = playList;
    this.mimeType = mimeType;
    this.sampleRate = sampleRate;
  }

  @Override
  public void addToParent(final LinearLayout parent, final boolean hasAlready) {
    View view = LayoutInflater.from(context).inflate(R.layout.chat_list_item_audio_content, null);

    utterMessageView = (TextView) view.findViewById(R.id.chat_msg_content_title);
    backgroundImage = (ImageView) view.findViewById(R.id.background_image);
    pauseDrawable = context.getResources().getDrawable(R.drawable.ic_stop);
    playDrawable = context.getResources().getDrawable(R.drawable.ic_play);
    playPause = (ImageButton) view.findViewById(R.id.play_pause);
    skipNext = (ImageButton) view.findViewById(R.id.skipNext);
    skipPrev = (ImageButton) view.findViewById(R.id.skipPrev);
    startTime = (TextView) view.findViewById(R.id.startText);
    endTime = (TextView) view.findViewById(R.id.endText);
    playSeekBar = (SeekBar) view.findViewById(R.id.seekBar1);
    musicTitleLineFirst = (TextView) view.findViewById(R.id.line1);
    musicTitleLineSecond = (TextView) view.findViewById(R.id.line2);
    musicTitleLineThird = (TextView) view.findViewById(R.id.line3);
    loadingProgressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
    controllersView = view.findViewById(R.id.controllers);

    playPause.setOnClickListener(this.onClickListener);
    skipNext.setOnClickListener(this.onClickListener);
    skipPrev.setOnClickListener(this.onClickListener);
    playSeekBar.setOnTouchListener(this.onTouchListener);

    // 대화 응답 메시지 존재시.
    if (utterMessage != null && utterMessage.trim().length() > 0) {
      utterMessageView.setText(utterMessage);
    }

    backgroundImage.setImageDrawable(context.getResources().getDrawable(R.drawable.beatles_bg));

    String musicTitle = getPlayTitleWithRemovedUrl(playList.get(0).toString());
    musicTitleLineFirst.setText(musicTitle);
    musicTitleLineSecond.setText(musicTitle);
    startTime.setText("0:00");
    endTime.setText("0:00");
    playSeekBar.setProgress(0);

    // Previous child view remove.
    if (hasAlready == true) {
      parent.removeAllViews();
    }
    // Long click listener 등록.
    if (onLongClickListener != null) {
      view.setOnLongClickListener(onLongClickListener);
      view.setTag(getMe());
    }
    // Add to parent.
    parent.addView(view);
  }

  /**
   * Player onClick listener.
   */
  private View.OnClickListener onClickListener = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      String nextMusic;
      if (state == STOP_STATE) {
        musicPlayer.setAudioPlayContent((ChatAudioContent) getMe());
      }
      switch (v.getId()) {
        case R.id.play_pause:
          if (state == STOP_STATE) {
            musicPlayer.playAndPause(true);
          } else {
            musicPlayer.playAndPause(false);
          }
          break;
        case R.id.skipNext:
          nextMusic = getPlayByNext(true);
          musicPlayer.next(nextMusic);
          break;
        case R.id.skipPrev:
          nextMusic = getPlayByNext(false);
          musicPlayer.prev(nextMusic);
          break;
        default:
          break;
      }
    }
  };

  /**
   * Play Control 의 TouchEvent 관리 Listener.
   * 사용처 : Seekbar 이동 disable.
   */
  private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {
      return true;//Seekbar 이동 disable.
    }
  };

  /**
   * 현재 컨텐츠의 play 상태를 리턴한다.
   */
  public int getState() {
    return state;
  }

  /**
   * Play control 의 상태에 따른 처리를 한다.
   * @param state play control state.
   */
  public void changeState(int state) {
    this.state = state;
    switch (this.state) {
      case STOP_STATE:
        stop();
        break;
      case MusicPlayer.PLAYING_STATE:
        play();
        break;
      default:
        break;
    }
  }

  /**
   * Mimetype getter.
   * @return Mimetype string.
   */
  public String getMimeType() {
    return mimeType;
  }

  /**
   * Mimetype setter.
   * @param mimeType Mimetype string.("audio/mp3")
   */
  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  /**
   * Audio samplerate getter.
   * @return Samplate string.
   */
  public String getSampleRate() {
    return sampleRate;
  }

  /**
   * Audio samplerate setter.
   * @param sampleRate Samplerate string.
   */
  public void setSampleRate(String sampleRate) {
    this.sampleRate = sampleRate;
  }

  /**
   * Music player 를 등록한다.
   * @param musicPlayer
   */
  public void setMusicPlayer(MusicPlayer musicPlayer) {
    this.musicPlayer = musicPlayer;
  }

  /**
   * Play list 를 저장한다.
   * @param playList
   */
  public void setPlayList(ArrayList<String> playList) {
    if (playList == null || playList.size() == 0) {
      return;
    }

    // 재생 목록을 설정한다.
    this.playList = playList;
    // 최초는 첫번째 곡을 Play 한다.
    currentPlayIndex = 0;
  }

  public String getPlayCurrent() {
    String nextPlayMusic = null;

    // 재생할 목록이 있는지 판단한다.
    if (playList == null || playList.size() == 0) {
      return null;
    }

    nextPlayMusic = playList.get(currentPlayIndex);

    return nextPlayMusic;
  }

  /**
   * 다음 재생할 곡이 있는지 판단한다.
   * @param nextOrPrev True is next music.False is previous music.
   * @return Null 은 다음 재생할 곡이 없음을 의미한다.
   *          다음 재생할 곡이 있으면 playlist 에서 해당 곡 정보를 리턴한다.
   */
  public String getPlayByNext(boolean nextOrPrev) {
    String nextPlayMusic = null;

    // 재생할 목록이 있는지 판단한다.
    if (playList == null || playList.size() == 0) {
      return null;
    }

    // 다음 재생할 index.
    int nextIndex;
    // nextPlayMusic true : 다음곡을 재생한다.
    // nextPlayMusic false : 이전곡을 재생한다.
    if (nextOrPrev == true) {
      nextIndex = currentPlayIndex;
      nextIndex++;
      // Array size 와 같을 경우 다음 곡이 없음으로 간주한다.
      if (nextIndex < playList.size()) {
        nextPlayMusic = playList.get(nextIndex).toString();
        currentPlayIndex = nextIndex;
      }
    } else {
      nextIndex = currentPlayIndex;
      nextIndex--;
      // Array size 와 같을 경우 다음 곡이 없음으로 간주한다.
      if (nextIndex >= 0) {
        nextPlayMusic = playList.get(nextIndex).toString();
        currentPlayIndex = nextIndex;
      }
    }

    return nextPlayMusic;
  }

  /**
   * 현재 재생 목록의 index 를 리턴한다.
   * @return 현재 재생 목록 index.-1 은 재생할 곡이 없음을 의미한다.
   */
  public int getCurrentPlayIndex() {
    return currentPlayIndex;
  }

  /**
   * 현재 재생 목록을 증가시킨다.
   * @param currentPlayIndex
   */
  public void setCurrentPlayIndex(int currentPlayIndex) {
    this.currentPlayIndex = currentPlayIndex;
  }

  /**
   * 시작시 play control 을 재설정한다.
   */
  private void play() {
    this.playPause.setImageDrawable(this.pauseDrawable);
    String musicTitle = getPlayTitleWithRemovedUrl(playList.get(this.currentPlayIndex).toString());
    musicTitleLineFirst.setText(musicTitle);
    musicTitleLineSecond.setText(musicTitle);
  }

  /**
   * 정지시 play control 을 재설정한다.
   */
  private void stop() {
    this.playPause.setImageDrawable(this.playDrawable);
    this.state = MusicPlayer.STOP_STATE;
  }

  /**
   * Play seekbar 의 max 시간을 설정한다.
   * @param value seekbar max value.
   */
  public void setPlaySeekBarMax(int value) {
    playSeekBar.setMax(value);

    double timeFinalTime = value;
    long remainMinute = TimeUnit.MILLISECONDS.toMinutes((long) timeFinalTime);
    long remainMiliToSeconds = TimeUnit.MILLISECONDS.toSeconds((long) timeFinalTime);
    long remainMiliToMinutes = TimeUnit.MILLISECONDS.toMinutes((long) timeFinalTime);
    long reminuteSeconds = remainMiliToSeconds - TimeUnit.MINUTES.toSeconds(remainMiliToMinutes);
    String timeFinalTimeString = String.format("%02d:%02d", remainMinute, reminuteSeconds);

    endTime.setText(timeFinalTimeString);
  }

  /**
   * Play 하는 동안 seekbar 를 업데이트 한다.
   * @param elapseTime Elapse time
   */
  public void setPlaySeekBar(int elapseTime) {
    playSeekBar.setProgress(elapseTime);
  }

  /**
   * Player 의 재생된 시간을 업데이트하여 남은 시간을 표시한다.
   * @param remainTime Elapse time.
   */
  public void updatePlayTime(String remainTime) {
    startTime.setText(remainTime);
  }

  /**
   * URL 제거된 재생 곡 명 가져오기.
   * @param url audio url string.
   * @return URL 제거된 재생 곡 제목.
   */
  public String getPlayTitleWithRemovedUrl(String url) {
    String playTitle = url;
    if (url == null) {
      return playTitle;
    }

    String[] splitArray = url.split("/");
    if (splitArray.length > 1) {
      int lastIndex = splitArray.length - 1 ;
      playTitle = splitArray[lastIndex];
    }

    return playTitle;
  }


}
