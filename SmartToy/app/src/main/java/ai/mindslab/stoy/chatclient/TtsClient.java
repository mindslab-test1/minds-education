package ai.mindslab.stoy.chatclient;

import static ai.mindslab.stoy.chatclient.NetConfigInfo.GRPC_HOST_ADDRESS;
import static ai.mindslab.stoy.chatclient.NetConfigInfo.GRPC_PORT;

import android.content.Context;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import elsa.facade.Dialog;
import elsa.facade.SpeechToTextServiceGrpc;
import elsa.facade.TextToSpeechServiceGrpc;
import elsa.facade.TextToSpeechServiceGrpc.TextToSpeechServiceStub;
import io.grpc.Channel;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.okhttp.OkHttpChannelBuilder;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;

/**
 * GRPC TTS Client.
 */
public class TtsClient {
  private final String TAG = TtsClient.class.getSimpleName();

  private Context context;
  private TextToSpeechServiceStub textToSpeechServiceStub;
  private ManagedChannel originalChannel;

  Long sessionId_;
  private static final Integer audioSampleRate = 16000;

  public enum ChatLanguage {
    kor,
    eng
  }

  private ChatLanguage chatLanguage = ChatLanguage.kor;


  /**
   * Constructor
   * @param context context
   * @param mg metadata getter instance.
   */
  public TtsClient(Context context, IMetadataGetter mg) {
    final Channel underlyingChannel;

    this.context = context;
    originalChannel = OkHttpChannelBuilder.forAddress(GRPC_HOST_ADDRESS,
        GRPC_PORT).usePlaintext(true).build();

    HeaderClientInterceptor headerClientInterceptor = new HeaderClientInterceptor();
    headerClientInterceptor.setMetadataGetter(mg);

    underlyingChannel = ClientInterceptors.intercept(originalChannel, headerClientInterceptor);
    textToSpeechServiceStub = TextToSpeechServiceGrpc.newStub(underlyingChannel);
  }

  /**
   * Grpc shutdown.
   * @throws InterruptedException
   */
  public void shutdown() throws InterruptedException {
    originalChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
  }

  /**
   * Speech to text.
   * @param text Text string.
   * @param lang current language.
   * @param ac audio callback.
   */
  public void getSenderTts(String text, final ChatLanguage lang, final IAudioCallback ac) {
    /* Set meta data */
    Metadata.Key<String> provider =
        Metadata.Key.of("x-elsa-authentication-provider", Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> audioHz =
        Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> language =
        Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
    Metadata header = new Metadata();
    header.put(provider, "mindslab");
    header.put(audioHz, "" + audioSampleRate);
    header.put(language, lang.name());

    textToSpeechServiceStub = MetadataUtils.attachHeaders(textToSpeechServiceStub, header);
    Log.d(TAG, "[GRPC Header]" + header.toString());

    elsa.facade.Dialog.TextUtter.Builder builder = Dialog.TextUtter.newBuilder();
    builder.setUtter(text);
    Dialog.TextUtter utter = builder.build();

    /* Request getServiceGroup */
    textToSpeechServiceStub.translate(utter, new StreamObserver<Dialog.AudioUtter>() {
      @Override
      public void onNext(Dialog.AudioUtter value) {
        ac.onAudio(value.getUtter());
      }

      @Override
      public void onError(Throwable t) {
        System.out.println("audio talk onError():" + t.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("audio talk onCompleted()");
        ac.onAudioEnd();
      }
    });
  }

  /**
   * Get chatLanguage.
   */
  public ChatLanguage getChatLanguage() {
    return chatLanguage;
  }

  /**
   * Set chatLanguage.
   */
  public void setChatLanguage(ChatLanguage chatLanguage) {
    this.chatLanguage = chatLanguage;
  }
}


