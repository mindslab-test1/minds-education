package ai.mindslab.stoy.chatclient;

import com.google.protobuf.ByteString;

/**
 * AudioTalk interface.
 */
public interface IAudioCallback {
  /**
   * Audiotalk repsone.
   * @param utter audio utter data.
   */
  void onAudio(ByteString utter);

  /**
   * Audiotalk respone end.
   */
  void onAudioEnd();
}
