package ai.mindslab.stoy.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ai.mindslab.stoy.R;
import ai.mindslab.stoy.model.ChatListItem;

/**
 * Classes for chat list itemview.
 */
public class ChatListItemView extends RelativeLayout {

  private Context context;
  private View rcvMessageView;
  private LinearLayout rcvMessageViewContent;
  private TextView rcvMessageTime;
  private View sendMessageView;
  private LinearLayout sendMessageViewContent;
  private TextView sendMessageTime;
  private ImageView receiveUserIcon;
  private ImageView sendUserIcon;

  /**
   * Constructor
   * @param context base context.
   */
  public ChatListItemView(Context context) {
    this(context, null);
  }

  public ChatListItemView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public ChatListItemView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    LayoutInflater.from(context).inflate(R.layout.chat_list_item_view, this, true);
    initView(context);
  }

  /**
   * User custom function.
   * @param context base context.
   */
  private void initView(Context context) {
    this.context = context;
    setupViewHolder();
  }

  public ChatListItemView inflate(ViewGroup parent) {
    ChatListItemView view =
        (ChatListItemView) LayoutInflater
            .from(parent.getContext()).inflate(R.layout.chat_list_item_row, parent, false);
    return view;
  }

  /**
   * Setup view holder.
   */
  private void setupViewHolder() {
    rcvMessageView = findViewById(R.id.list_rcv_msg_type_ll);
    rcvMessageViewContent = (LinearLayout) findViewById(R.id.rcv_msg_content_ll);
    rcvMessageTime = (TextView) findViewById(R.id.rcv_msg_time);
    receiveUserIcon = (ImageView) findViewById(R.id.rcv_user_icon);

    sendMessageView = findViewById(R.id.list_send_msg_type_ll);
    sendMessageViewContent = (LinearLayout) findViewById(R.id.send_msg_content_ll);
    sendMessageTime = (TextView) findViewById(R.id.send_msg_time);
    sendUserIcon = (ImageView) findViewById(R.id.send_user_icon);
  }

  /**
   * Set layout data.
   * @param data menu item object.
   * @param hasAlready True is the target view has already.
   */
  public void setData(Object data, boolean hasAlready) {
    if (data != null) {
      ChatListItem item = (ChatListItem) data;
      ChatMetaObject chatMetaObject = item.getChatMetaObject();
      LinearLayout targetContent;

      // Set meta object by message type.
      if (item.getType() == ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_TEXT_TYPE
          || item.getType() == ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_HTML_TYPE
          || item.getType() == ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_AUDIO_TYPE
          || item.getType() == ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_LINK_TYPE
          || item.getType() == ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_VIDEO_TYPE) {
        rcvMessageView.setVisibility(View.VISIBLE);
        sendMessageView.setVisibility(View.GONE);
        rcvMessageTime.setText(chatMetaObject.getMessageTime());
        targetContent = rcvMessageViewContent;
      } else if (item.getType() == ChatListItem.ChatItemType.CHAT_SEND_MESSAGE_TYPE) {
        rcvMessageView.setVisibility(View.GONE);
        sendMessageView.setVisibility(View.VISIBLE);
        sendMessageTime.setText(chatMetaObject.getMessageTime());
        targetContent = sendMessageViewContent;
      } else {
        // Error!!Not Supported type.
        return;
      }

      // Add the meta object to parent view.
      chatMetaObject.addToParent(targetContent, hasAlready);
    }
  }
}
