package ai.mindslab.stoy.media;

import android.content.Context;
import android.widget.Toast;

import ai.mindslab.stoy.view.ChatAudioContent;

public class MusicPlayer {
  private Context context;
  private ChatAudioContent audioPlayContent = null;
  private OnCustomEventListener onCustomEventListener = null;

  public static final int STOP_STATE = 0;
  public static final int PLAYING_STATE = 1;

  public MusicPlayer(Context context) {
    this.context = context;
  }

  public ChatAudioContent getAudioPlayContent() {
    return audioPlayContent;
  }

  /**
   * 재생 컨트롤에 대한 playlist control 에 대한 정보를 셋팅한다.
   * @param audioPlayContent ChatAudioContent type 의 object.
   */
  public void setAudioPlayContent(ChatAudioContent audioPlayContent) {
    if (audioPlayContent == null)
      return;

    // 이잰 재생 Play Content 가 있을 경우 이전 PlayControl 의 동작을 멈춘다.
    if (this.audioPlayContent != null) {
      this.audioPlayContent.changeState(STOP_STATE);
    }
    // Play 할 컨텐츠를 등록한다.
    this.audioPlayContent = audioPlayContent;
    // Play index 를 첫번째 곡으로 초기화한다.
    this.audioPlayContent.setCurrentPlayIndex(0);
  }

  /**
   * 재생할 곡에 대해 play and pause 를 한다.
   * @param playAndPause true 값이면 play. false 값이면 pause 를 한다.
   */
  public void playAndPause(boolean playAndPause) {
    if (audioPlayContent == null) {
      return;
    }

    if (playAndPause == true) {
      if (this.onCustomEventListener != null) {
        String nextMusic = audioPlayContent.getPlayCurrent();
        if (nextMusic == null) {
          Toast.makeText(context, "다음 재생할 곡이 없습니다.", Toast.LENGTH_LONG).show();
          return;
        }
        this.onCustomEventListener.onStartPlayer(nextMusic);
        audioPlayContent.changeState(PLAYING_STATE);
      }
    } else {
      if (this.onCustomEventListener != null) {
        this.onCustomEventListener.onStopPlayer();
        audioPlayContent.changeState(STOP_STATE);
      }
    }
  }

  /**
   * 다음 곡을 재생한다.
   * @param nextMusic 재생할 곡 정보.(URI 정보)
   */
  public void next(String nextMusic) {
    if (audioPlayContent == null) {
      return;
    }

    if (this.onCustomEventListener != null) {
      if (nextMusic == null) {
        Toast.makeText(context, "다음 재생할 곡이 없습니다.", Toast.LENGTH_LONG).show();
        return;
      }
      this.onCustomEventListener.onStartPlayer(nextMusic);
      audioPlayContent.changeState(PLAYING_STATE);
    }
  }

  /**
   * 이전 곡을 재생한다.
   * @param nextMusic 재생할 곡 정보.(URI 정보)
   */
  public void prev(String nextMusic) {
    if (audioPlayContent == null) {
      return;
    }

    if (this.onCustomEventListener != null) {
      if (nextMusic == null) {
        Toast.makeText(context, "이전 재생할 곡이 없습니다.", Toast.LENGTH_LONG).show();
        return;
      }
      this.onCustomEventListener.onStartPlayer(nextMusic);
      audioPlayContent.changeState(PLAYING_STATE);
    }
  }

  /**
   * 현재 등록된 미디어 컨텐츠의 play 상태를 리턴한다.
   * @return  MusicPlayer 의 playing 상태를 리턴하여 준다.
   */
  public int getMusicPlayerState() {
    int state = STOP_STATE;

    if (audioPlayContent != null) {
      state = audioPlayContent.getState();
    }

    return state;
  }

  /**
   * Play seekbar 의 max 시간을 설정한다.
   * @param value seekbar max value.
   */
  public void setPlaySeekBarMax(int value) {
    if (audioPlayContent == null) {
      return;
    }

    this.audioPlayContent.setPlaySeekBarMax(value);
  }

  /**
   * Play 하는 동안 seekbar 를 업데이트 한다.
   * @param elapseTime Elapse time
   */
  public void updateSeekBar(int elapseTime) {
    if (audioPlayContent == null) {
      return;
    }

    this.audioPlayContent.setPlaySeekBar(elapseTime);
  }

  /**
   * Player 의 재생된 시간을 업데이트하여 남은 시간을 표시한다.
   * @param remainTime Elapse time.
   */
  public void updatePlayTime(String remainTime) {
    if (audioPlayContent == null) {
      return;
    }

    this.audioPlayContent.updatePlayTime(remainTime);
  }

  /**
   * Custom Event handler interface.
   */
  public interface OnCustomEventListener {
    void onStartPlayer(String nextMusic);
    void onStopPlayer();
  }

  /**
   * Custom Event handler setter.
   * @param eventListener event listener.
   */
  public void setCustomEventListener(OnCustomEventListener eventListener) {
    onCustomEventListener = eventListener;
  }

}
