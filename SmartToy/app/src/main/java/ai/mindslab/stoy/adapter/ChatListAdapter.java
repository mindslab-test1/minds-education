package ai.mindslab.stoy.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import ai.mindslab.stoy.model.ChatListItem;
import ai.mindslab.stoy.view.ChatListItemView;

/**
 * Chatting Screen ListAdaptoer.
 */
public class ChatListAdapter extends BaseAdapter {
  private Context context;
  private ArrayList<ChatListItem> items;

  /**
   * Constructor
   * @param context base context.
   */
  public ChatListAdapter(Context context) {
    this.context = context;
    items = new ArrayList<ChatListItem>();
  }

  @Override
  public int getCount() {
    if (this.items != null) {
      return this.items.size();
    }
    return 0;
  }

  @Override
  public ChatListItem getItem(int i) {
    return this.items.get(i);
  }

  @Override
  public long getItemId(int i) {
    return 0;
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {
    ChatListItemView itemView;
    boolean hasAlready = false;
    if (view != null && view instanceof ChatListItemView) {
      itemView = (ChatListItemView) view;
      hasAlready = true;
    } else {
      itemView = new ChatListItemView(context).inflate(viewGroup);
    }
    itemView.setData(getItem(i), hasAlready);

    return itemView;
  }

  /**
   * User custom function.
   * @param items ChatListItem type ArrayList.
   */
  public void setItems(ArrayList<ChatListItem> items) {
    this.items = items;
    notifyDataSetChanged();
  }
}
