package ai.mindslab.stoy.model;

import ai.mindslab.stoy.view.ChatMetaObject;


/**
 * Chatlist item.
 */
public class ChatListItem {
  public enum ChatItemType{
    CHAT_NONE_MESSAGE_TYPE,
    CHAT_SEND_MESSAGE_TYPE,
    CHAT_RCV_MESSAGE_TEXT_TYPE,
    CHAT_RCV_MESSAGE_HTML_TYPE,
    CHAT_RCV_MESSAGE_AUDIO_TYPE,
    CHAT_RCV_MESSAGE_LINK_TYPE,
    CHAT_RCV_MESSAGE_VIDEO_TYPE
  };

  private ChatItemType type;
  private String userImage;
  private ChatMetaObject chatMetaObject;

  /**
   * Constuctor
   * @param type chatting message type.
   * @param userImage chatbot icon image url.
   * @param chatMetaObject ChatMetaObject type object.
   */
  public ChatListItem(ChatItemType type, String userImage, ChatMetaObject chatMetaObject) {
    this.type = type;
    this.userImage = userImage;
    this.chatMetaObject = chatMetaObject;
  }

  /**
   * chatting message type getter.
   * @return chatting message integer type.
   */
  public ChatItemType getType() {
    return type;
  }

  /**
   * chatting message type setter.
   * @param type chatting message integer type.
   */
  public void setType(ChatItemType type) {
    this.type = type;
  }

  /**
   * chatbot icon image url getter.
   * @return image url string.
   */
  public String getUserImage() {
    return userImage;
  }

  /**
   * chatbot icon image url setter.
   * @param userImage image url string.
   */
  public void setUserImage(String userImage) {
    this.userImage = userImage;
  }

  /**
   * chat message string getter.
   * @return ChatMetaObject type object.
   */
  public ChatMetaObject getChatMetaObject() {
    return chatMetaObject;
  }

  /**
   * chat message string setter.
   * @param chatMetaObject ChatMetaObject type object.
   */
  public void setChatMetaObject(ChatMetaObject chatMetaObject) {
    this.chatMetaObject = chatMetaObject;
  }
}
