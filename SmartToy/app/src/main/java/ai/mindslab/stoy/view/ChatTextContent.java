package ai.mindslab.stoy.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ai.mindslab.stoy.R;

/**
 * Classes for ChatTextContent container.
 */
public class ChatTextContent extends ChatMetaObject {
  private Context context;
  private String textData;

  private TextView messageTitleTextView;

  /**
   * Constructor
   * @param context base context.
   */
  public ChatTextContent(Context context, String textData) {
    this.context = context;
    this.textData = textData;
  }

  @Override
  public void addToParent(final LinearLayout parent, final boolean hasAlready) {
    View view = LayoutInflater.from(context).inflate(R.layout.chat_list_item_text_content, null);
    messageTitleTextView = (TextView) view.findViewById(R.id.chat_msg_content_title);

    messageTitleTextView.setText(this.textData);

    // Previous child view remove.
    if (hasAlready == true) {
      parent.removeAllViews();
    }
    // Long click listener 등록.
    if (onLongClickListener != null) {
      view.setOnLongClickListener(onLongClickListener);
      view.setTag(getMe());
    }
    // Add to parent.
    parent.addView(view);
  }

  /**
   * Text string getter.
   * @return Test string.
   */
  public String getTextData() {
    return textData;
  }

  /**
   * Text string setter.
   * @param textData Text string.
   */
  public void setTextData(String textData) {
    this.textData = textData;
  }
}
