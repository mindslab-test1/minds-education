package ai.mindslab.stoy.view;

/*
 * Created by hybell on 2016. 12. 27..
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.graphics.Bitmap;
import android.os.Message;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import ai.mindslab.stoy.R;

/**
 * Classes for ChatHtmlContent container.
 */
public class ChatHtmlContent extends ChatMetaObject {
  private Context context;
  private String htmlStyle;
  private String htmlBody;

  private TextView utterMessageView;
  private WebView webView;
  /**
   * Constructor
   * @param context base context.
   */
  public ChatHtmlContent(Context context, String htmlStyle, String htmlBody) {
    this.context = context;
    this.htmlStyle = htmlStyle;
    this.htmlBody = htmlBody;
  }

  /**
   * WebViewClient
   */
  protected final WebViewClient webViewClient = new WebViewClient() {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
      view.loadUrl(url);
      return true;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
      super.onPageStarted(view, url, favicon);
    }
    @Override
    public void onPageFinished(WebView view, String url) {
      super.onPageFinished(view, url);
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
      super.onReceivedError(view, errorCode, description, failingUrl);
    }

    @Override
    public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
      super.doUpdateVisitedHistory(view, url, isReload);
    }

    @Override
    public void onFormResubmission(WebView view, Message dontResend, Message resend) {
      super.onFormResubmission(view, dontResend, resend);
      if (view.canGoBack()) {
        view.goBack();
      }
    }

    @Override
    public void onLoadResource(WebView view, String url) {
      super.onLoadResource(view, url);
    }
  };

  @Override
  public void addToParent(final LinearLayout parent, final boolean hasAlready) {
    View view = LayoutInflater.from(context).inflate(R.layout.chat_list_item_html_content, null);
    utterMessageView = (TextView) view.findViewById(R.id.chat_msg_content_title);
    webView = (WebView) view.findViewById(R.id.chat_html_content_webview);

    // 대화 응답 메시지 존재시.
    if (utterMessage != null && utterMessage.trim().length() > 0) {
      utterMessageView.setText(utterMessage);
    }

    webView.setWebViewClient(webViewClient);
    WebSettings settings = webView.getSettings();
    settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
    settings.setJavaScriptEnabled(true);
    settings.setLoadsImagesAutomatically(true);
    settings.setSaveFormData(false);
    webView.setVerticalScrollBarEnabled(false);
    webView.setHorizontalScrollBarEnabled(false);

    webView.loadData(this.htmlBody, "text/html", "utf-8");

    // Previous child view remove.
    if (hasAlready == true) {
      parent.removeAllViews();
    }
    // Long click listener 등록.
    if (onLongClickListener != null) {
      view.setOnLongClickListener(onLongClickListener);
      view.setTag(getMe());
    }
    // Add to parent.
    parent.addView(view);
  }

  /**
   * Html style getter.
   * @return Html style string.
   */
  public String getHtmlStyle() {
    return htmlStyle;
  }

  /**
   * Html style setter.
   * @param htmlStyle  Html style string.
   */
  public void setHtmlStyle(String htmlStyle) {
    this.htmlStyle = htmlStyle;
  }

  /**
   * Html body getter.
   * @return Html body string.
   */
  public String getHtmlBody() {
    return htmlBody;
  }

  /**
   * Html body setter.
   * @param htmlBody Html body string.
   */
  public void setHtmlBody(String htmlBody) {
    this.htmlBody = htmlBody;
  }
}
