package ai.mindslab.stoy.preference;

/**
 * Broadcast Action define.
 */
public interface BroadcastAction {
  /**
   * 메시지 수신
   */
  static final String ACTION_MESSAGE_RECEIVED = "ai.mindslab.stoy.intent.action.ACTION_MESSAGE_RECEIVED";
  /**
   * FCM Regstration ID Get Event
   */
  static final String REGISTRATION_COMPLETE = "ai.mindslab.stoy.intent.action.REGISTRATION_COMPLETE";
}
