package ai.mindslab.stoy.activity;

import static ai.mindslab.stoy.model.ChatListItem.ChatItemType.CHAT_NONE_MESSAGE_TYPE;
import static ai.mindslab.stoy.model.ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_AUDIO_TYPE;
import static ai.mindslab.stoy.model.ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_HTML_TYPE;
import static ai.mindslab.stoy.model.ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_LINK_TYPE;
import static ai.mindslab.stoy.model.ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_TEXT_TYPE;
import static ai.mindslab.stoy.model.ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_VIDEO_TYPE;
import static ai.mindslab.stoy.model.ChatListItem.ChatItemType.CHAT_SEND_MESSAGE_TYPE;
import static android.R.id.message;

import com.google.protobuf.ByteString;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import ai.mindslab.stoy.R;
import ai.mindslab.stoy.adapter.ChatListAdapter;
import ai.mindslab.stoy.chatclient.IAudioCallback;
import ai.mindslab.stoy.chatclient.IMetadataGetter;
import ai.mindslab.stoy.chatclient.ITextCallback;
import ai.mindslab.stoy.chatclient.TtsClient;
import ai.mindslab.stoy.media.G711;
import ai.mindslab.stoy.model.ChatListItem;
import ai.mindslab.stoy.model.ChatListItem.ChatItemType;
import ai.mindslab.stoy.model.DecodeMetaData;
import ai.mindslab.stoy.model.StoreListItem;
import ai.mindslab.stoy.preference.BroadcastAction;
import ai.mindslab.stoy.preference.IntentConstants;
import ai.mindslab.stoy.view.ChatAudioContent;
import ai.mindslab.stoy.view.ChatHtmlContent;
import ai.mindslab.stoy.view.ChatLinkContent;
import ai.mindslab.stoy.view.ChatMetaObject;
import ai.mindslab.stoy.view.ChatTextContent;
import elsa.facade.Dialog;
import io.grpc.Metadata;
import io.grpc.stub.StreamObserver;

/**
 * Classes for ChatActivity
 */
public class ChatActivity extends ChatBase {
  public static final String EXTRA_SVC_GROUP_INFO = "ai.mindslab.chatapp.activity.ExtraSvcGroupInfo";

  private static final String TAG = ChatActivity.class.getSimpleName();
  private StoreListItem servieGroupInfo;
  private ArrayList<ChatListItem> chatListItems = new ArrayList<ChatListItem>();
  private ChatListAdapter chatListAdapter;
  private ReceiveMetaData receiveDecodeMetaData = new ReceiveMetaData();

  private ImageButton addButton;
  private EditText messageEditText;
  private ImageButton voiceButton;
  private Button sendMessageButton;
  private SimpleDateFormat messageDateFormat;

  /* 음성 요청으로 Text 응답을 받을지 여부를 설정한다. */
  private boolean audioToTextOn = false;
  /* FCM Message Event Receiver */
  private BroadcastReceiver broadcastReceiver;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    //Start DM Client Service
    Intent i = new Intent();
    i.setClassName("com.mds.lwm2mclient", "com.mds.lwm2mclient.MainService");
    this.startService(i);

    getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

    // Param 으로 넘어온 ServiceGroupKey 를 셋팅한다.
    /*Intent intent = getIntent();
    servieGroupInfo = (StoreListItem) intent.getSerializableExtra(EXTRA_SVC_GROUP_INFO);*/
    servieGroupInfo = new StoreListItem("", "kids_pal", "", "kids_pal");

    // Set Main Layout xml.
    setContentView(R.layout.activity_chat);

    // Initialize chat list view.
    ListView listView = (ListView) findViewById(R.id.chat_list_view);
    chatListAdapter = new ChatListAdapter(this);
    listView.setAdapter(chatListAdapter);

    // Initialize other view & control
    addButton = (ImageButton) findViewById(R.id.chat_add_btn);
    messageEditText = (EditText) findViewById(R.id.chat_text_et);
    voiceButton = (ImageButton) findViewById(R.id.chat_voice_btn);
    sendMessageButton = (Button) findViewById(R.id.chat_send_btn);

    //addButton.setOnClickListener(onClickListener);
    //voiceButton.setOnClickListener(onClickListener);
    sendMessageButton.setOnClickListener(onClickListener);

    addButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        /*// [START subscribe_topics]
        FirebaseMessaging.getInstance().subscribeToTopic("news");
        // [END subscribe_topics]

        // Log and toast
        String msg = getString(R.string.msg_subscribed);
        Log.d(TAG, msg);
        Toast.makeText(ChatActivity.this, "news topic send", Toast.LENGTH_SHORT).show();*/

        Intent msgIntent = new Intent(BroadcastAction.ACTION_MESSAGE_RECEIVED);
        msgIntent.putExtra(IntentConstants.FCM_RECEIVE_MSG_TEXT, "오늘 날씨 어때?");
        sendBroadcast(msgIntent);
        Log.d(TAG, "Send Message Broadcast!!");
      }
    });

    voiceButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

      }
    });

    // Message date format.
    messageDateFormat = new SimpleDateFormat("a HH:mm");

    // Initialize client and set meta data receive handler
    setupChatClient(servieGroupInfo.getBigName());

    broadcastReceiver = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        Log.d(TAG, "onReceive:" + action);
        // FCM Message 수신.
        if(action.equals(BroadcastAction.ACTION_MESSAGE_RECEIVED)){
          Log.d(TAG, "Received ACTION_MESSAGE_RECEIVED..");
          String strMessage = intent.getStringExtra(IntentConstants.FCM_RECEIVE_MSG_TEXT);
          Log.d(TAG, "Receive Msg:" + strMessage);
          messageEditText.setText(strMessage);
          sendTextMessage();
        }
      }
    };
    registerReceiver(broadcastReceiver, new IntentFilter(BroadcastAction.ACTION_MESSAGE_RECEIVED));

    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    String token = prefs.getString("Token", null);
    Log.i(TAG, "[FCM Token] " + token);

    // TODO : Test Example remove.
    boolean isTest = false;
    if (isTest == true) {
      createTestCode();
    }
  }

  /**
   * For Test Chat Content Code.
   */
  public void createTestCode() {
    String sendMessage = "오늘 날씨 어때?";
    String receiveMessage = "구름 조금, 온도는 20도. 운동하기 좋은 날씨 입니다.";
    ChatItemType chatItemType;
    Calendar calendar = Calendar.getInstance();
    String messageTime = messageDateFormat.format(calendar.getTime());

    // 전송 메시지.
    chatItemType = CHAT_SEND_MESSAGE_TYPE;
    ChatTextContent textContent = new ChatTextContent(getBaseContext(), sendMessage);
    textContent.setMessageTime(messageTime);
    textContent.setOnLongClickListener(onLongClickListener);
    textContent.chatItemType = chatItemType;
    chatListItems.add(new ChatListItem(chatItemType, null, textContent));

    // Video 수신 메시지.
    chatItemType = CHAT_RCV_MESSAGE_VIDEO_TYPE;
    textContent = new ChatTextContent(getBaseContext(), "file://video.mp4");
    textContent.setMessageTime(messageTime);
    textContent.setOnLongClickListener(onLongClickListener);
    textContent.chatItemType = chatItemType;
    chatListItems.add(new ChatListItem(chatItemType, null, textContent));

    // TEXT 수신 메시지.
    chatItemType = CHAT_RCV_MESSAGE_TEXT_TYPE;
    textContent = new ChatTextContent(getBaseContext(), receiveMessage);
    textContent.setMessageTime(messageTime);
    textContent.setOnLongClickListener(onLongClickListener);
    textContent.chatItemType = chatItemType;
    chatListItems.add(new ChatListItem(chatItemType, null, textContent));

    // HTML Type 메시지
    chatItemType = CHAT_RCV_MESSAGE_HTML_TYPE;
    String htmlStyle = "";
    String htmlBody = "<html><body>HTML Test Message</br>Hellow!!</body></html>";
    ChatHtmlContent chatHtmlContent = new ChatHtmlContent(getBaseContext(), htmlStyle, htmlBody);
    chatHtmlContent.setUtterMessage("EmbedOutText : Test");
    chatHtmlContent.setMessageTime(messageTime);
    chatHtmlContent.setOnLongClickListener(onLongClickListener);
    chatHtmlContent.chatItemType = chatItemType;
    chatListItems.add(new ChatListItem(chatItemType, null, chatHtmlContent));

    // Link Type 메시지
    chatItemType = CHAT_RCV_MESSAGE_LINK_TYPE;
    ChatLinkContent chatLinkContent =
        new ChatLinkContent(getBaseContext(), "http://naver.com");
    chatLinkContent.setUtterMessage("EmbedOutText : Test");
    chatLinkContent.setDescription("Description : Naver 입니다.");
    chatLinkContent.setTitle("안녕하세요. Naver 입니다.");
    chatLinkContent.setMessageTime(messageTime);
    chatLinkContent.setOnLongClickListener(onLongClickListener);
    chatLinkContent.chatItemType = chatItemType;
    chatListItems.add(new ChatListItem(chatItemType, null, chatLinkContent));

    // Audio 수신 메시지.
    chatItemType = CHAT_RCV_MESSAGE_AUDIO_TYPE;
    String mimeType = "audio/mp3";
    ArrayList<String> playList = new ArrayList<String>();
    playList.add("http://211.172.25.49:8090/mindslab/test2.mp3");
    ChatAudioContent chatAudioContent =
        new ChatAudioContent(getBaseContext(), playList, mimeType, "44100");
    chatAudioContent.setUtterMessage("EmbedOutText : Test");
    chatAudioContent.setMessageTime(messageTime);
    chatAudioContent.setMusicPlayer(musicPlayer);
    chatAudioContent.setOnLongClickListener(onLongClickListener);
    chatAudioContent.chatItemType = chatItemType;
    chatListItems.add(new ChatListItem(chatItemType, null, chatAudioContent));

    // Audio 수신 메시지.
    chatItemType = CHAT_RCV_MESSAGE_AUDIO_TYPE;
    playList = new ArrayList<String>();
    playList.add("http://211.172.25.49:8090/mindslab/test1.mp3");
    playList.add("http://211.172.25.49:8090/mindslab/test2.mp3");
    chatAudioContent = new ChatAudioContent(getBaseContext(), playList, mimeType, "44100");
    chatAudioContent.setUtterMessage("EmbedOutText : Test");
    chatAudioContent.setMessageTime(messageTime);
    chatAudioContent.setMusicPlayer(musicPlayer);
    chatAudioContent.setOnLongClickListener(onLongClickListener);
    chatAudioContent.chatItemType = chatItemType;
    chatListItems.add(new ChatListItem(chatItemType, null, chatAudioContent));

    // Setting chat list adaptor.
    chatListAdapter.setItems(chatListItems);
  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    Log.i(TAG, "Down KeyCode = " + keyCode);

    switch(keyCode) {
      case KeyEvent.KEYCODE_VOICE_ASSIST:
      case KeyEvent.KEYCODE_VOLUME_UP:
 //       Toast.makeText(getBaseContext(), "DOWN VOICE_ASSIST", Toast.LENGTH_SHORT).show();

        voiceListeningDialog.show();
        startRecording();
        chatbotState = SLEEPING;
        processKey("얘초롱아", (short)1000);
        return true;
    }

    return super.onKeyDown(keyCode, event);
  }

  @Override
  protected void onDestroy() {
    /**
     * 앱 종료되기 전이나 종료하기 전에 GCMRegistrar.onDestroy를 반드시 호출한다.
     * 호출하지 않을 경우 unRegisterReceiver오류가 발생한다.
     * 해당 함수는 null이나 기타 오류에 대해 내부적으로 예외 처리하고 있으므로, 아무때나 마음껏 호출해도 된다.
     */
//    GCMRegistrar.onDestroy(getApplicationContext());

    // Broadcast 해제
    unregisterReceiver(broadcastReceiver);

    // TODO Auto-generated method stub
    super.onDestroy();
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
  }

  @Override
  protected boolean addAudioToTextTalkData(String text) {
    Log.i(TAG, "TextUtter Callback:" + text);
    Calendar calendar = Calendar.getInstance();
    final String messageTime = messageDateFormat.format(calendar.getTime());
    ChatItemType chatItemType;

    // '얘 초롱아' 등 Wakeup keyword 에 의한 Response 메시지가 없는 경우. 빈 대화 메시지 방지 체크.
    /*
    if (text == null && text.trim().length() > 0) {
      return false;
    }*/

    // 메타 데이터 존재여부 체크한다.
    if (receiveDecodeMetaData.hasDecodeMetaData() == true) {
      ChatMetaObject metaObject = null;
      DecodeMetaData decodeMetaData = receiveDecodeMetaData.getDecodedMetatdata();
      // Intext message field check.
      String sendMessage = decodeMetaData.getInText();

      // '얘 초롱아' 등 Wakeup keyword 에 의한 Response 메시지가 없는 경우. 빈 대화 메시지 방지 체크.
      if (sendMessage != null && sendMessage.trim().length() > 0) {
        // Outembed type 미생성시 text chat object 로 생성한다.
        // Audio Talk 대화 시만 존재하여, Send Message Type 으로 지정한다.
        ChatTextContent textContent = new ChatTextContent(getBaseContext(), sendMessage);
        textContent.setMessageTime(messageTime);
        textContent.setOnLongClickListener(onLongClickListener);
        textContent.chatItemType = CHAT_SEND_MESSAGE_TYPE;
        chatListItems.add(new ChatListItem(textContent.chatItemType, null, textContent));

        // Update chat list.
        chatListAdapter.setItems(chatListItems);
      }

      // OutText Message check.
      //String receiveMessage = decodeMetaData.getOutText();
      String receiveMessage = text;
      if (decodeMetaData.getOutEmbedType() != null) {
        // Embed type 존재시 outtext message 를 1개의 채팅아이템에 결합한다.
        metaObject = receiveDecodeMetaData.getEmbedMetaDataObject();
        metaObject.setMessageTime(messageTime);
        if (receiveMessage != null) {
          metaObject.setUtterMessage(receiveMessage);
        }
        metaObject.setOnLongClickListener(onLongClickListener);
        chatListItems.add(new ChatListItem(metaObject.chatItemType, null, metaObject));
      } else {
        // Outtext 만 존재시 receive text message 형태의 채팅 아이템으로 보여준다.
        ChatTextContent textContent = new ChatTextContent(getBaseContext(), receiveMessage);
        textContent.setMessageTime(messageTime);
        textContent.setOnLongClickListener(onLongClickListener);
        textContent.chatItemType = CHAT_RCV_MESSAGE_TEXT_TYPE;
        chatListItems.add(new ChatListItem(textContent.chatItemType, null, textContent));
      }

      // Clear for next received meta data container.
      receiveDecodeMetaData.clear();

      // Update chat list.
      chatListAdapter.setItems(chatListItems);
    }

    return true;
  }

  @Override
  protected boolean addAudioTalkData() {
    Calendar calendar = Calendar.getInstance();
    final String messageTime = messageDateFormat.format(calendar.getTime());
    ChatItemType chatItemType;

    // 메타 데이터 존재여부 체크한다.
    if (receiveDecodeMetaData.hasDecodeMetaData() == true) {
      ChatMetaObject metaObject = null;
      DecodeMetaData decodeMetaData = receiveDecodeMetaData.getDecodedMetatdata();
      // Intext message field check.
      String sendMessage = decodeMetaData.getInText();

      // '얘 초롱아' 등 Wakeup keyword 에 의한 Response 메시지가 없는 경우. 빈 대화 메시지 방지 체크.
      if (sendMessage != null && sendMessage.trim().length() > 0) {
        // Outembed type 미생성시 text chat object 로 생성한다.
        // Audio Talk 대화 시만 존재하여, Send Message Type 으로 지정한다.
        ChatTextContent textContent = new ChatTextContent(getBaseContext(), sendMessage);
        textContent.setMessageTime(messageTime);
        textContent.setOnLongClickListener(onLongClickListener);
        textContent.chatItemType = CHAT_SEND_MESSAGE_TYPE;
        chatListItems.add(new ChatListItem(textContent.chatItemType, null, textContent));

        // Update chat list.
        chatListAdapter.setItems(chatListItems);
      }

      // Check OutEmbedType.
      String receiveMessage = decodeMetaData.getOutText();
      if (TextUtils.isEmpty(decodeMetaData.getOutEmbedType()) == false) {
        // Embed type 존재시 outtext message 를 1개의 채팅아이템에 결합한다.
        metaObject = receiveDecodeMetaData.getEmbedMetaDataObject();
        metaObject.setMessageTime(messageTime);
        if (TextUtils.isEmpty(receiveMessage) == false) {
          metaObject.setUtterMessage(receiveMessage);
        }
        metaObject.setOnLongClickListener(onLongClickListener);
//        chatListItems.add(new ChatListItem(metaObject.chatItemType, null, metaObject));

        chatItemType = metaObject.chatItemType;
        String outEmbedUrl = decodeMetaData.getOutEmbedDataUrl();
        // Verify outEmbedUrl
        if (TextUtils.isEmpty(outEmbedUrl) == false) {
          if (chatItemType == CHAT_RCV_MESSAGE_VIDEO_TYPE) {
            Uri uri = Uri.parse(outEmbedUrl);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(uri, "video/*");
            startActivity(intent);
          } else if (chatItemType == CHAT_RCV_MESSAGE_AUDIO_TYPE) {
            Uri uri = Uri.parse(outEmbedUrl);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(uri, "audio/*");
            startActivity(intent);
          }
        }
      } else {
        // Outtext 만 존재시 receive text message 형태의 채팅 아이템으로 보여준다.
        ChatTextContent textContent = new ChatTextContent(getBaseContext(), receiveMessage);
        textContent.setMessageTime(messageTime);
        textContent.setOnLongClickListener(onLongClickListener);
        textContent.chatItemType = CHAT_RCV_MESSAGE_TEXT_TYPE;
        chatListItems.add(new ChatListItem(textContent.chatItemType, null, textContent));
      }

      // Clear for next received meta data container.
      receiveDecodeMetaData.clear();

      // Update chat list.
      chatListAdapter.setItems(chatListItems);
    }

    return true;
  }

  @Override
  protected boolean addChatData(final DecodeMetaData decodeMetaData) {
    receiveDecodeMetaData.setDecodedMetatdata(decodeMetaData);

    return true;
  }

  @Override
  protected boolean checkAudioToText() {
    return audioToTextOn;
  }

  /**
   * Chat message OnLongClick listener.
   */
  private View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
    @Override
    public boolean onLongClick(View v) {
      Log.d(TAG, "Show context menu");
      ChatMetaObject chatMetaObject = (ChatMetaObject)v.getTag();

      showContextMenuDialog(chatMetaObject);
      return true;
    }
  };

  /**
   * Clipboard 에 string 을 저장하다.
   * @param text 저장하고자 하는 텍스트.
   */
  private void saveToClipboard(String text) {
    if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
      android.text.ClipboardManager clipboard =
          (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
      clipboard.setText(text);
    } else {
      android.content.ClipboardManager clipboard =
          (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
      android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
      clipboard.setPrimaryClip(clip);
    }

    Toast.makeText(getBaseContext(), "클립보드에 저장하였습니다.", Toast.LENGTH_SHORT).show();
  }

  /**
   * Chatbot context menu dialog 를 보여준다.
   * @param chatMetaObject meta object instance.
   */
  private void showContextMenuDialog(final ChatMetaObject chatMetaObject) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setItems(R.array.chatbot_context_menu, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            String sessionSting;

            // The 'which' argument contains the index position
            // of the selected item
            switch (which) {
              case 0://Share.
                String subject = "MINDsLab";
                String text = getSessionString(chatMetaObject);

                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                intent.putExtra(Intent.EXTRA_TEXT, text);
                Intent chooser = Intent.createChooser(intent, "타이틀");
                startActivity(chooser);
                break;
              case 1://Copy.
                // Get all session string.
                sessionSting = getAllSessionString();
                // Save to clipboard.
                saveToClipboard(sessionSting);
                break;
              case 2://Copy this session.
                // Get all session string.
                sessionSting = getAllSessionString();
                // Save to clipboard.
                saveToClipboard(sessionSting);
                break;
            }
          }
        });

    // create alert dialog
    AlertDialog alertDialog = builder.create();
    // show it
    alertDialog.show();
  }

  /**
   * Activity onClick listener.
   */
  private View.OnClickListener onClickListener = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      switch (v.getId()) {
        case R.id.chat_voice_btn:
          // Voice Listening Dialog show.
          voiceListeningDialog.show();
          // start recording
          startRecording();
          // '얘 초롱아' 를 다시 보낸다.
          chatbotState = SLEEPING;
          processKey("얘초롱아", (short)1000);
          break;
        case R.id.chat_send_btn:
          sendTextMessage();
          break;
        default:
          break;
      }
    }
  };

  /**
   * Text message send and response data add to chat list.
   */
  private void sendTextMessage() {
    Calendar calendar = Calendar.getInstance();
    final String messageTime = messageDateFormat.format(calendar.getTime());
    final String sendMessage = messageEditText.getText().toString();
    /*
    if (!sendMessage.trim().isEmpty()) {
      StreamObserver<Dialog.TextUtter> sender = chatClient.getSender(new ITextCallback() {
        @Override
        public void onText(final String receiveMessage) {
          runOnUiThread(new Runnable() {
            @Override
            public void run() {
              Log.i(TAG, "TextUtter Callback:" + receiveMessage);

              DecodeMetaData decodeMetaData = receiveDecodeMetaData.getDecodedMetatdata();
              // 메타 데이터 존재여부 체크한다.
              if (receiveDecodeMetaData.hasDecodeMetaData() == true
                  && decodeMetaData.getOutEmbedType() != null) {
                // Embed type 존재시 response message 를 1개의 채팅아이템에 결합한다.
                ChatMetaObject metaObject = receiveDecodeMetaData.getEmbedMetaDataObject();
                metaObject.setMessageTime(messageTime);
                if (receiveMessage != null) {
                  metaObject.setUtterMessage(receiveMessage);
                }
                metaObject.setOnLongClickListener(onLongClickListener);
                chatListItems.add(new ChatListItem(metaObject.chatItemType, null, metaObject));
              } else {
                // Outtext 만 존재시 receive text message 형태의 채팅 아이템으로 보여준다.
                ChatTextContent textContent = new ChatTextContent(getBaseContext(), receiveMessage);
                textContent.setMessageTime(messageTime);
                textContent.setOnLongClickListener(onLongClickListener);
                textContent.chatItemType = CHAT_RCV_MESSAGE_TEXT_TYPE;
                chatListItems.add(new ChatListItem(textContent.chatItemType, null, textContent));
              }

              // Update chat list.
              chatListAdapter.setItems(chatListItems);
            }
          });
        }
      });
      elsa.facade.Dialog.TextUtter.Builder builder = Dialog.TextUtter.newBuilder();
      builder.setUtter(sendMessage);
      Dialog.TextUtter utter = builder.build();
      sender.onNext(utter);
      sender.onCompleted();
*/


    TtsClient ttsClient = new TtsClient(getBaseContext(), new IMetadataGetter() {
      @Override
      // Meta data receiver
      public void onMetaData(Metadata md) {
        Log.d(TAG, md.toString());
      }
    });

    ttsClient.getSenderTts(sendMessage, TtsClient.ChatLanguage.kor, new IAudioCallback() {
      short[] ab = new short[minBuffSizeInShort];

      /** Audio incoming */
      @Override
      public void onAudio(ByteString utter) {
        byte[] bytes = utter.toByteArray();
        System.out.printf(">>>>>>>>>>>>>>>>>>>>> %d byte reached\n", bytes.length);
        short data[] = new short[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
          data[i] = (short) G711.ulaw2linear(bytes[i]);
          //writeData_[writePosition_++] = data[i];
          ab[writePosition_++] = data[i];
          if (writePosition_ == minBuffSizeInShort) {
            writePosition_ = 0;
            replies_.add(ab);
            ab = new short[minBuffSizeInShort];
          }
        }
      }

      /** Incoming stream end */
      @Override
      public void onAudioEnd() {
        System.out.printf("audio end\n");
        if (writePosition_ > 0) {
          replies_.add(ab);
          ab = new short[minBuffSizeInShort];
        }
        runOnUiThread(new Runnable() {
          public void run() {
            // Listening dialog hide.
            voiceListeningDialog.hide();
            // Add the audio meta data.
            boolean result = addAudioTalkData();
          }
        });
      }
    });

    ChatItemType chatItemType = CHAT_SEND_MESSAGE_TYPE;
    ChatTextContent textContent = new ChatTextContent(getBaseContext(), sendMessage);
    textContent.setMessageTime(messageTime);
    textContent.setOnLongClickListener(onLongClickListener);
    textContent.chatItemType = chatItemType;
    chatListItems.add(new ChatListItem(chatItemType, null, textContent));
    chatListAdapter.setItems(chatListItems);
    messageEditText.setText("");

    // 수신받은 Audio 를 재생한다.
    startPlaying();
  }

  /**
   * Get all session message text.
   * @return session string.
   */
  private String getAllSessionString() {
    String sessionString = "";

    for (int i = 0; i < chatListItems.size(); i++) {
      ChatListItem chatListItem = chatListItems.get(i);
      ChatItemType itemType = chatListItem.getType();
      switch (itemType) {
        case CHAT_SEND_MESSAGE_TYPE:
          sessionString += "\n" + ((ChatTextContent)chatListItem.getChatMetaObject()).getTextData();
          break;
        case CHAT_RCV_MESSAGE_TEXT_TYPE:
          sessionString += "\n" + ((ChatTextContent)chatListItem.getChatMetaObject()).getTextData();
          break;
        case CHAT_RCV_MESSAGE_HTML_TYPE:
          break;
        case CHAT_RCV_MESSAGE_AUDIO_TYPE:
          break;
        case CHAT_RCV_MESSAGE_LINK_TYPE:
          break;
      }
    }

    return sessionString;
  }

  /**
   * Get session message text.
   * @return session string.
   */
  private String getSessionString(ChatMetaObject chatMetaObject) {
    String sessionString = "";

    ChatItemType itemType = chatMetaObject.chatItemType;
    switch (itemType) {
      case CHAT_SEND_MESSAGE_TYPE:
        sessionString += "\n" + ((ChatTextContent)chatMetaObject).getTextData();
        break;
      case CHAT_RCV_MESSAGE_TEXT_TYPE:
        sessionString += "\n" + ((ChatTextContent)chatMetaObject).getTextData();
        break;
      case CHAT_RCV_MESSAGE_HTML_TYPE:
        break;
      case CHAT_RCV_MESSAGE_AUDIO_TYPE:
        break;
      case CHAT_RCV_MESSAGE_LINK_TYPE:
        break;
    }

    return sessionString;
  }

  /**
   * 수신된 Meta data object container.
   */
  private class ReceiveMetaData {
    private boolean isSet = false;
    private DecodeMetaData decodedMetatdata = null;

    /**
     * Has received meta data object set.
     * @return True is success.Else is failure.
     */
    public boolean hasDecodeMetaData() {
      return this.isSet;
    }

    /**
     * Clear meta data object.
     */
    public void clear() {
      this.isSet = false;
    }

    /**
     * Decode metadata getter.
     * @return Decode metadata.
     */
    public DecodeMetaData getDecodedMetatdata() {
      return decodedMetatdata;
    }

    /**
     * Decode metadata setter.
     * @param decodedMetatdata Decode meta data.
     */
    public void setDecodedMetatdata(DecodeMetaData decodedMetatdata) {
      if (decodedMetatdata == null)
        return;

      this.decodedMetatdata = decodedMetatdata;
      isSet = true;
    }

    /**
     * Get metaobject from decoded meta data.
     * @return Parsed meta object.
     */
    public ChatMetaObject getEmbedMetaDataObject() {
      ChatItemType chatItemType = CHAT_NONE_MESSAGE_TYPE;
      ChatMetaObject metaObject = null;
      DecodeMetaData decodeMetaData = this.decodedMetatdata;

      // Check decode meta data.
      if (decodeMetaData == null) {
        return null;
      }

      // Add talk data to chat list.
      String outEmbedType = decodeMetaData.getOutEmbedType();
      if ("html5".equals(outEmbedType)) {
        String dataStyle = decodeMetaData.getOutEmbedDataStyle();
        String dataBody = decodeMetaData.getOutEmbedDataBody();
        metaObject = new ChatHtmlContent(getBaseContext(), dataStyle, dataBody);
        metaObject.setOnLongClickListener(onLongClickListener);
        metaObject.chatItemType = CHAT_RCV_MESSAGE_HTML_TYPE;
      } else if ("audio".equals(outEmbedType)) {;
        String dataUrl = decodeMetaData.getOutEmbedDataUrl();
        String mimeType = decodeMetaData.getOutEmbedDataMime();
        String sampleRate = decodeMetaData.getOutEmbedDataSamplerate();

        ArrayList<String> playList = new ArrayList<String>();
        playList.add(dataUrl);
        ChatAudioContent chatAudioContent =
            new ChatAudioContent(getBaseContext(), playList, mimeType, sampleRate);
        chatAudioContent.setMusicPlayer(musicPlayer);
        chatAudioContent.setOnLongClickListener(onLongClickListener);
        chatAudioContent.chatItemType = CHAT_RCV_MESSAGE_AUDIO_TYPE;
        metaObject = chatAudioContent;
      } else if ("link".equals(outEmbedType)) {
        metaObject = new ChatLinkContent(getBaseContext(), decodeMetaData.getOutEmbedDataUrl());
        metaObject.setOnLongClickListener(onLongClickListener);
        metaObject.chatItemType = CHAT_RCV_MESSAGE_LINK_TYPE;
      } else if ("video".equals(outEmbedType)) {
        metaObject = new ChatTextContent(getBaseContext(), decodeMetaData.getOutEmbedDataUrl());
        metaObject.setOnLongClickListener(onLongClickListener);
        metaObject.chatItemType = CHAT_RCV_MESSAGE_VIDEO_TYPE;
      }

      // Intext message field check.
      if (decodeMetaData.getInText() != null) {
        if (metaObject == null) {
          // Outembed type 미생성시 text chat object 로 생성한다.
          // Audio Talk 대화 시만 존재하여, Send Message Type 으로 지정한다.
          metaObject = new ChatTextContent(getBaseContext(), decodeMetaData.getInText());
          metaObject.chatItemType = CHAT_SEND_MESSAGE_TYPE;
        } else {
          metaObject.setIntextMessage(decodeMetaData.getInText());
        }
      }

      return metaObject;
    }
  }
}
