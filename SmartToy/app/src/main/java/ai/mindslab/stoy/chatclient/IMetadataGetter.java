package ai.mindslab.stoy.chatclient;

import io.grpc.Metadata;

public interface IMetadataGetter {

    void onMetaData(Metadata md);
}
