package ai.mindslab.stoy.chatclient;

/**
 * TextTalk response interface.
 */
public interface ITextCallback {

  /**
   * Text result interface.
   * @param text reponse text.
   */
  void onText(String text);
}
