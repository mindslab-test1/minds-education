package ai.mindslab.stoy.preference;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Classes for Preference Helper.
 */
public class PreferenceHelper {
  // 환경설정 저장 파일
  public final static String AI_CLOUD_CHATAPP_PREFERENCES = "ai_cloud_chatapp_preferences";
  // Access Token
  public final static String KEY_NETWORK_ACCESS_TOKEN = "netowrk_access_token";
  // GPS 위도값
  public final static String KEY_LAST_LOCATION_LATITUDE = "last_latitude";
  // GPS 경도값
  public final static String KEY_LAST_LOCATION_LONGITUDE	= "last_longitude";

  /**
   * Integer value getter.
   * @param context context
   * @param key key
   * @return integer value
   */
  public static int getIntKeyValue(Context context, String key) {
    SharedPreferences sp = context.getSharedPreferences(AI_CLOUD_CHATAPP_PREFERENCES, Context.MODE_PRIVATE);
    return sp.getInt(key, 0);
  }

  /**
   * String value getter.
   * @param context context
   * @param key key
   * @return string value
   */
  public static String getStringKeyValue(Context context, String key) {
    SharedPreferences sp = context.getSharedPreferences(AI_CLOUD_CHATAPP_PREFERENCES, Context.MODE_PRIVATE);
    return sp.getString(key, "");
  }

  /**
   * boolean value getter.
   * @param context context
   * @param key key
   * @return boolean value
   */
  public static boolean getBooleanKeyValue(Context context, String key) {
    SharedPreferences sp = context.getSharedPreferences(AI_CLOUD_CHATAPP_PREFERENCES, Context.MODE_PRIVATE);
    return sp.getBoolean(key, false);
  }

  /**
   * Integer value setter.
   * @param context context
   * @param key key
   * @param value value
   */
  public static void setIntKeyValue(Context context, String key, int value) {
    SharedPreferences sp = context.getSharedPreferences(AI_CLOUD_CHATAPP_PREFERENCES, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sp.edit();
    editor.putInt(key, value);
    editor.commit();
  }

  /**
   * String value setter.
   * @param context context
   * @param key key
   * @param value value
   */
  public static void setStringKeyValue(Context context, String key, String value) {
    SharedPreferences sp = context.getSharedPreferences(AI_CLOUD_CHATAPP_PREFERENCES, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sp.edit();
    editor.putString(key, value);
    editor.commit();
  }

  /**
   * boolean value setter.
   * @param context context
   * @param key key
   * @param value value
   */
  public static void setBooleanKeyValue(Context context, String key, boolean value) {
    SharedPreferences sp = context.getSharedPreferences(AI_CLOUD_CHATAPP_PREFERENCES, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sp.edit();
    editor.putBoolean(key, value);
    editor.commit();
  }
}
