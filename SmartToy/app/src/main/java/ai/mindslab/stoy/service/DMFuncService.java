package ai.mindslab.stoy.service;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.mds.neofalcon.aidl.IDMFunc;

import org.json.JSONException;
import org.json.JSONObject;

import ai.mindslab.stoy.preference.BroadcastAction;
import ai.mindslab.stoy.preference.IntentConstants;

public class DMFuncService extends Service {

	private static final String TAG = "DMFuncService";

	public DMFuncService() {

	}

	@Override
	public IBinder onBind(Intent intent) {
		return mDMFuncBinder;
	}

	private final IDMFunc.Stub mDMFuncBinder = new IDMFunc.Stub() {
		@Override
		public void set_DisplayText(String value) throws RemoteException {
			Log.i(TAG, "Message : " + value);

			try {
				JSONObject jsonObject = new JSONObject(value);

				String type = jsonObject.optString("type");
				String message = jsonObject.optString("message");

				if ("1".equals(type)) {
					Intent intent = new Intent(BroadcastAction.ACTION_MESSAGE_RECEIVED);
					intent.putExtra(IntentConstants.FCM_RECEIVE_MSG_TEXT, message);
					sendBroadcast(intent);
				} else if ("2".equals(type)) {
					Uri uri = Uri.parse(message);
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.setDataAndType(uri, "video/*");
					startActivity(intent);
				} else if ("3".equals(type)) {
					Uri uri = Uri.parse(message);
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.setDataAndType(uri, "audio/*");
					startActivity(intent);
				}
			} catch (JSONException e) {
				Log.e(TAG, "JSONException", e);
			} catch (Exception e) {
				Log.e(TAG, "Exception", e);
			}

		}
	};
}
