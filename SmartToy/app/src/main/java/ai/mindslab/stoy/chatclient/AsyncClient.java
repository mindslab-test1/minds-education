package ai.mindslab.stoy.chatclient;

import static ai.mindslab.stoy.chatclient.NetConfigInfo.GRPC_HOST_ADDRESS;
import static ai.mindslab.stoy.chatclient.NetConfigInfo.GRPC_PORT;

import com.google.common.util.concurrent.SettableFuture;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import ai.mindslab.stoy.R;
import elsa.facade.Dialog.AccessFrom;
import elsa.facade.Dialog.AudioUtter;
import elsa.facade.Dialog.Caller;
import elsa.facade.Dialog.Session;
import elsa.facade.Dialog.SessionKey;
import elsa.facade.Dialog.SessionStat;
import elsa.facade.Dialog.TextUtter;
import elsa.facade.DialogServiceGrpc;
import elsa.facade.DialogServiceGrpc.DialogServiceStub;
import io.grpc.Channel;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.okhttp.OkHttpChannelBuilder;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;

/**
 * Base chat client.
 */
public class AsyncClient {
  private Context context;
  private DialogServiceStub asyncStub;
  private ManagedChannel originalChannel;

  Long sessionId;

  private static final Integer audioSampleRate = 16000;

  private String serviceGroup = "";

  public enum ChatLanguage {
    kor,
    eng
  }

  private ChatLanguage chatLanguage = ChatLanguage.kor;

  /**
   * Constructor
   */
  public AsyncClient(Context context, IMetadataGetter mg) {
    final Channel underlyingChannel;

    this.context = context;
    originalChannel = OkHttpChannelBuilder.forAddress(GRPC_HOST_ADDRESS,
        GRPC_PORT).usePlaintext(true).build();

    HeaderClientInterceptor headerClientInterceptor = new HeaderClientInterceptor();
    headerClientInterceptor.setMetadataGetter(mg);

    underlyingChannel = ClientInterceptors.intercept(originalChannel, headerClientInterceptor);
    asyncStub = DialogServiceGrpc.newStub(underlyingChannel);
  }

  /**
   * Grpc shutdown.
   */
  public void shutdown() throws InterruptedException {
    originalChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
  }

  /**
   * Grpc open with servicegroup.
   */
  public void open() {
    open(ChatLanguage.kor, serviceGroup); //default
  }

  /**
   * Grpc open with language.
   */
  public void open(ChatLanguage lang) {
    open(lang, serviceGroup);
  }

  /**
   * Grpc open
   */
  public void open(final ChatLanguage lang, String svcGroup) {
    final SettableFuture<Void> finishFuture = SettableFuture.create();
    Caller.Builder builder = Caller.newBuilder();
    builder.setName("Smart Toy");
    builder.setAccessFrom(AccessFrom.MOBILE_ANDROID);
    builder.setSvcGroup(svcGroup);
    Caller accessCaller = builder.build();

    //blockingStub_.open(accessCaller);
    asyncStub.open(accessCaller, new StreamObserver<Session>() {
      @Override
      public void onNext(Session session) {
        sessionId = session.getId();

        Metadata.Key<String> sessionKey =
            Metadata.Key.of("in.sessionid", Metadata.ASCII_STRING_MARSHALLER);
        Metadata.Key<String> audioHz =
            Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
        Metadata.Key<String> language =
            Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
        Metadata.Key<String> provider =
            Metadata.Key.of("x-elsa-authentication-provider", Metadata.ASCII_STRING_MARSHALLER);

        Metadata header = new Metadata();
        header.put(sessionKey, "" + sessionId);
        header.put(audioHz, "" + audioSampleRate);
        header.put(language, lang.name());
        header.put(provider, "mindslab");
        asyncStub = MetadataUtils.attachHeaders(asyncStub, header);
        chatLanguage = lang;
        Log.d("======================", header.keys().toString());
      }

      @Override
      public void onError(Throwable err) {
        err.printStackTrace();
        finishFuture.setException(err);
      }

      @Override
      public void onCompleted() {
        System.out.println("stub open completed");
        finishFuture.set(null);
      }
    });

    try {
      finishFuture.get(10, TimeUnit.SECONDS);
    } catch (Exception e) {
      String errorMessage = this.context.getString(R.string.error_grpc_connection_fail);
      Toast.makeText(this.context, errorMessage, Toast.LENGTH_SHORT).show();
      e.printStackTrace();
    }
  }

  /**
   * AudioTalk
   */
  public StreamObserver<AudioUtter> getSender(final IAudioCallback ac) {
    return asyncStub.audioTalk(new StreamObserver<AudioUtter>() {
      @Override
      public void onNext(AudioUtter utter) {
        ac.onAudio(utter.getUtter());
      }

      @Override
      public void onError(Throwable err) {
      }

      @Override
      public void onCompleted() {
        System.out.println("audio talk completed");
        ac.onAudioEnd();
      }
    });
  }

  /**
   * TextTalk
   */
  public StreamObserver<TextUtter> getSender(final ITextCallback tc) {
    return asyncStub.textTalk(new StreamObserver<TextUtter>() {
      @Override
      public void onNext(TextUtter utter) {
        //System.out.println("utter:"+utter.getUtter());
        tc.onText(utter.getUtter());
      }

      @Override
      public void onError(Throwable err) {
      }

      @Override
      public void onCompleted() {
        System.out.println("text talk completed");
      }
    });
  }

  /**
   * AudioTextTalk
   */
  public StreamObserver<AudioUtter> getSender(final IAudioTextCallback atc) {
    return asyncStub.audioToTextTalk(new StreamObserver<TextUtter>() {
      @Override
      public void onNext(TextUtter utter) {
        //System.out.println("utter:"+utter.getUtter());
        atc.onText(utter.getUtter());
      }

      @Override
      public void onError(Throwable err) {
      }

      @Override
      public void onCompleted() {
        System.out.println("audio-text talk completed");
      }
    });
  }

  /**
   * Close asyncClient.
   */
  public void close() {
    SessionKey key = SessionKey.getDefaultInstance();
    //SessionStat stat = blockingStub_.close(key);
    asyncStub.close(key, new StreamObserver<SessionStat>() {
      @Override
      public void onNext(SessionStat stat) {
        // 여기서 대답을 얻는다.
        System.out.println("stub close code : " + stat.getResultCode());
      }

      @Override
      public void onError(Throwable err) {
        err.printStackTrace();
      }

      @Override
      public void onCompleted() {
      }
    });
  }

  /**
   * 서비스 그룹을 셋팅한다.
   * @param serviceGroup
   */
  public void setServiceGroup(String serviceGroup) {
    this.serviceGroup = serviceGroup;
  }

  /**
   * Get chatLanguage.
   */
  public ChatLanguage getChatLanguage() {
    return chatLanguage;
  }

  /**
   * Set chatLanguage.
   */
  public void setChatLanguage(ChatLanguage chatLanguage) {
    this.chatLanguage = chatLanguage;
  }
}


