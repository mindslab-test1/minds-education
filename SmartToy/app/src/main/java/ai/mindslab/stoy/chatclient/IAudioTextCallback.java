package ai.mindslab.stoy.chatclient;

/**
 * AudioToText talk interface.
 */
public interface IAudioTextCallback {
  /**
   * AudioText talk response.
   * @param text text response.
   */
  void onText(String text);
}
