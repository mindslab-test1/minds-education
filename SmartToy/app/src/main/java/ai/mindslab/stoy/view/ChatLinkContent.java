package ai.mindslab.stoy.view;

/*
 * Created by hybell on 2016. 12. 27..
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ai.mindslab.stoy.R;

public class ChatLinkContent extends ChatMetaObject {
  private Context context;
  private String linkUrl;
  private String type;
  private String title;
  private String imageUrl;
  private String description;

  private TextView utterMessageView;
  private ImageView linkImageView;
  private TextView linkDescriptionTextView;
  private TextView linkUrlTextView;
  /**
   * Constructor
   * @param context base context.
   */
  public ChatLinkContent(Context context, String linkUrl) {
    this.context = context;
    this.linkUrl = linkUrl;
  }

  @Override
  public void addToParent(final LinearLayout parent, final boolean hasAlready) {
    View view = LayoutInflater.from(context).inflate(R.layout.chat_list_item_link_content, null);

    utterMessageView = (TextView) view.findViewById(R.id.chat_msg_content_title);
    linkImageView = (ImageView) view.findViewById(R.id.chat_link_content_image);
    linkDescriptionTextView = (TextView) view.findViewById(R.id.chat_link_content_description);
    linkUrlTextView = (TextView) view.findViewById(R.id.chat_link_content_url);

    // 대화 응답 메시지 존재시.
    if (utterMessage != null && utterMessage.trim().length() > 0) {
      utterMessageView.setText(utterMessage);
    }
    linkImageView.setImageDrawable(context.getDrawable(R.drawable.image_maum_ai));
    linkDescriptionTextView.setText(this.description);
    linkUrlTextView.setText(this.linkUrl);

    // Previous child view remove.
    if (hasAlready == true) {
      parent.removeAllViews();
    }
    // Long click listener 등록.
    if (onLongClickListener != null) {
      view.setOnLongClickListener(onLongClickListener);
      view.setTag(getMe());
    }
    // Add to parent.
    parent.addView(view);
  }

  /**
   * Link url getter.
   * @return Link url string.
   */
  public String getLinkUrl() {
    return linkUrl;
  }

  /**
   * Link url setter.
   * @param linkUrl
   */
  public void setLinkUrl(String linkUrl) {
    this.linkUrl = linkUrl;
  }

  /**
   * Link url type getter.
   * @return Link url type string.
   */
  public String getType() {
    return type;
  }

  /**
   * Link url type setter
   * @param type Link url type string.
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * Title 정보 getter.
   * @return Title 정보.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Title 정보 setter.
   * @param title Title 정보.
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * Image url getter.
   * @return Image url string.
   */
  public String getImageUrl() {
    return imageUrl;
  }

  /**
   * Image url setter.
   * @param imageUrl Image url string.
   */
  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  /**
   * Description 정보 getter.
   * @return Description 정보 string.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Description 정보 setter.
   * @param description Description 정보 string.
   */
  public void setDescription(String description) {
    this.description = description;
  }
}
