package ai.mindslab.stoy.view;

/*
 * Created by hybell on 2016. 12. 27..
 */

import android.view.View;
import android.widget.LinearLayout;

import ai.mindslab.stoy.model.ChatListItem;

public abstract class ChatMetaObject {
  // Message create time.
  public String messageTime = null;
  // Utter 응답 메시지.
  public String utterMessage = null;
  // STT 응답 메시지.in.text field message.
  public String intextMessage = null;
  // Metaobject type.
  public ChatListItem.ChatItemType chatItemType;
  // Metabobject long click linstener.
  public View.OnLongClickListener onLongClickListener = null;

  /**
   * Long click listener setter.
   * @param onLongClickListener
   */
  public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
    this.onLongClickListener = onLongClickListener;
  }

  /**
   * This object 를 리턴한다.
   * @return this object.
   */
  protected ChatMetaObject getMe() {
    return this;
  }

  /**
   * Message time getter.
   * @return Message time string.
   */
  public String getMessageTime() {
    return messageTime;
  }

  /**
   * Message time setter.
   * @param messageTime Message time string.
   */
  public void setMessageTime(String messageTime) {
    this.messageTime = messageTime;
  }

  /**
   * Utter message string getter.
   * @return Utter message string
   */
  public String getUtterMessage() {
    return utterMessage;
  }

  /**
   * Utter message string Setter.
   * @param utterMessage Utter message string
   */
  public void setUtterMessage(String utterMessage) {
    this.utterMessage = utterMessage;
  }

  /**
   * Intext message getter.
   * @return Intext message string.
   */
  public String getIntextMessage() {
    return intextMessage;
  }

  /**
   * Intext message setter.
   * @param intextMessage Intext message string.
   */
  public void setIntextMessage(String intextMessage) {
    this.intextMessage = intextMessage;
  }

  /**
   * Chat meta object 를 상위 View 에 Add 한다.
   * @param parent Parent view to add the view.
   * @param hasAlready True is the target view has already.
   */
  public abstract void addToParent(final LinearLayout parent, final boolean hasAlready);
}
