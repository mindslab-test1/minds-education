package ai.mindslab.stoy.model;

import java.io.Serializable;

/*
 * Created by pmomac2015 on 2016. 12. 16..
 */
public class SettingListItem implements Serializable{
  private String imageUrl;
  private String title;
  private String description;

  /**
   * Constructor
   * @param imageUrl image url
   * @param title item title
   * @param description description
   */
  public SettingListItem(String imageUrl, String title, String description) {
    this.imageUrl = imageUrl;
    this.title = title;
    this.description = description;
  }

  /**
   * Image url getter.
   * @return image url.
   */
  public String getImageUrl() {
    return imageUrl;
  }

  /**
   * Image url setter.
   * @param imageUrl image url.
   */
  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  /**
   * Title getter.
   * @return title.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Title setter.
   * @param title title.
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * Description getter.
   * @return Description string.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Description setter.
   * @param description Description string.
   */
  public void setDescription(String description) {
    this.description = description;
  }
}
