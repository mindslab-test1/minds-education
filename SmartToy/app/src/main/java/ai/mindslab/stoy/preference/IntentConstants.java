package ai.mindslab.stoy.preference;

/**
 * Intent 상수값을 정의한다.
 */
public class IntentConstants {
  /**
   * FCM Message
   */
  public final static String FCM_GET_REGID_MESSAGE = "ai.mindslab.stoy.Fcm.FCM_GET_REGID_MESSAGE";
  /**
   * FCM Message Time
   */
  public final static String FCM_RECEIVE_MSG_TIME	= "ai.mindslab.stoy.Fcm.FCM_RECEIVE_MSG_TIME";
  /**
   * FCM Message Text
   */
  public final static String FCM_RECEIVE_MSG_TEXT = "ai.mindslab.stoy.Fcm.FCM_RECEIVE_MSG_TEXT";

}
