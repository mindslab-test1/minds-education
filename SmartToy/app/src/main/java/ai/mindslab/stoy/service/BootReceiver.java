package ai.mindslab.stoy.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import ai.mindslab.stoy.activity.ChatActivity;

/**
 * BootReceiver broadcast receiver.
 */
public class BootReceiver extends BroadcastReceiver {
  @Override
  public void onReceive(Context context, Intent intent) {

    if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
      Intent i = new Intent(context, ChatActivity.class);
      context.startService(i);
    }

    Log.d("BootReceiver", "BootReceiver onReceived.");
  }
}