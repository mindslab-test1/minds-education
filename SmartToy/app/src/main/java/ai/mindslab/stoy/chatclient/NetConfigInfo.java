package ai.mindslab.stoy.chatclient;

/**
 * Created by hybell on 2017. 1. 23..
 */

public class NetConfigInfo {
  /* GRPC Server Address */
// public static final String GRPC_HOST_ADDRESS = "10.122.64.11";  // 내부 IP
//  public static final String GRPC_HOST_ADDRESS = "125.132.250.203";
  public static final String GRPC_HOST_ADDRESS = "demo.mindslab.ai";
  /* GRPC Server Port */
  public static final Integer GRPC_PORT = 9901;
}
