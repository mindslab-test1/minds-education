package ai.mindslab.stoy.model;

import java.io.Serializable;

/*
 * Created by pmomac2015 on 2016. 12. 16..
 */
public class StoreListItem implements Serializable{
  private String imageUrl;
  private String bigName;
  private String description;
  private String smallName;

  /**
   * Constructor
   * @param imageUrl image url
   * @param bigName big title name.
   * @param description description info.
   * @param smallName small title name.
   */
  public StoreListItem(String imageUrl, String bigName, String description, String smallName) {
    this.imageUrl = imageUrl;
    this.bigName = bigName;
    this.description = description;
    this.smallName = smallName;
  }

  /**
   * Image url getter.
   * @return image url string.
   */
  public String getImageUrl() {
    return imageUrl;
  }

  /**
   * Image url setter.
   * @param imageUrl image url string.
   */
  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  /**
   * Big name title getter.
   * @return title string.
   */
  public String getBigName() {
    return bigName;
  }

  /**
   * Big name title setter.
   * @param bigName title string.
   */
  public void setBigName(String bigName) {
    this.bigName = bigName;
  }

  /**
   * Item description getter.
   * @return description string.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Item description setter.
   * @param description description string.
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Small name getter.
   * @return title string.
   */
  public String getSmallName() {
    return smallName;
  }

  /**
   * Small name setter.
   * @param smallName title string.
   */
  public void setSmallName(String smallName) {
    this.smallName = smallName;
  }
}
