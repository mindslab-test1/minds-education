package org.altbeacon.beaconreference;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.Collection;

public class RangingActivity extends Activity implements BeaconConsumer, RangeNotifier {
    protected static final String TAG = "conner";
    private BeaconManager mBeaconManager;
    private TextView tvBe1Act, tvBe2Act, tvBe3Act;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranging);
        tvBe1Act = (TextView) findViewById(R.id.tv_be1_act);
        tvBe2Act = (TextView) findViewById(R.id.tv_be2_act);
        tvBe3Act = (TextView) findViewById(R.id.tv_be3_act);
        //mBeaconManager.bind(this);
    }

    @Override 
    protected void onDestroy() {
        super.onDestroy();
        mBeaconManager.unbind(this);
    }

    @Override 
    protected void onPause() {
        super.onPause();
        //if (beaconManager.isBound(this)) beaconManager.setBackgroundMode(true);
        mBeaconManager.unbind(this);
    }

    @Override 
    protected void onResume() {
        super.onResume();
        //if (beaconManager.isBound(this)) beaconManager.setBackgroundMode(false);
        mBeaconManager = BeaconManager.getInstanceForApplication(this.getApplicationContext());
        // Detect the URL frame:
        mBeaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout(BeaconParser.EDDYSTONE_URL_LAYOUT));
        mBeaconManager.bind(this);
    }

    @Override
    public void onBeaconServiceConnect() {
        /*beaconManager.setRangeNotifier(new RangeNotifier() {
           @Override
           public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
              if (beacons.size() > 0) {
                 //EditText editText = (EditText)RangingActivity.this.findViewById(R.id.rangingText);
                 Beacon firstBeacon = beacons.iterator().next();
                 logToDisplay("The first beacon " + firstBeacon.toString() + " is about " + firstBeacon.getDistance() + " meters away.");
              }
           }

        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (RemoteException e) {   }*/
        Region region = new Region("all-beacons-region", null, null, null);
        try {
            mBeaconManager.startRangingBeaconsInRegion(region);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        mBeaconManager.setRangeNotifier(this);
    }

    private void logToDisplay(final String line) {
        runOnUiThread(new Runnable() {
            public void run() {
                EditText editText = (EditText)RangingActivity.this.findViewById(R.id.rangingText);
                editText.append(line+"\n");
            }
        });
    }
    int act1, act2, act3;
    @Override
    public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
        for (Beacon beacon: beacons) {
            if (beacon.getServiceUuid() == 0xfeaa && beacon.getBeaconTypeCode() == 0x10) {
                // This is a Eddystone-URL frame
                //String url = UrlBeaconUrlCompressor.uncompress(beacon.getId1().toByteArray());
                String address = beacon.getBluetoothAddress().toString();
                Log.d("conner","address "+address);

                if(address.equals("B0:B4:48:F9:FB:D7")){
                    byte[] actCount = beacon.getId1().toByteArray();
                    Log.d("conner","beacon 1");
                    for(int j = 0;j< 2;j++){
                        Log.d("conner",""+actCount[j]);
                    }
                    if(actCount[0] == 0x00){
                        if(actCount[1] < 0){
                            act1 = (int)actCount[1]+256;
                        }else{
                            act1 = (int)actCount[1];
                        }
                    }
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable(){
                                @Override
                                public void run() {
                                    viewBeaocn1(act1);
                                }
                            });
                        }
                    }).start();
                }else if(address.equals("B0:B4:48:F9:FB:9F")){
                    byte[] actCount = beacon.getId1().toByteArray();
                    Log.d("conner","beacon 2");
                    for(int j = 0;j< 2;j++){
                        Log.d("conner",""+actCount[j]);
                    }
                    if(actCount[0] == 0x00){
                        if(actCount[1] < 0){
                            act2 = (int)actCount[1]+256;
                        }else{
                            act2 = (int)actCount[1];
                        }
                    }
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable(){
                                @Override
                                public void run() {
                                    viewBeaocn2(act2);
                                }
                            });
                        }
                    }).start();
                }else if(address.equals("B0:B4:48:F9:FB:50")){
                    byte[] actCount = beacon.getId1().toByteArray();
                    Log.d("conner","beacon 3");
                    for(int j = 0;j< 2;j++){
                        Log.d("conner",""+actCount[j]);
                    }
                    if(actCount[0] == 0x00){
                        if(actCount[1] < 0){
                            act3 = (int)actCount[1]+256;
                        }else{
                            act3 = (int)actCount[1];
                        }
                    }
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable(){
                                @Override
                                public void run() {
                                    viewBeaocn3(act3);
                                }
                            });
                        }
                    }).start();
                }

               /* String address = beacon.getBluetoothAddress().toString();
                if(address.equals("B0:B4:48:F9:FB:D7")){
                    Log.d("conner","Address"+address);
                    int start = url.indexOf('.');
                    int end = url.lastIndexOf('.');

                    String data = url.substring(start+1, end);
                    Log.d("conner","Data "+data);
                    byte[] temp = data.getBytes();
                    for(int h=0;h<temp.length;h++){
                        Log.d("conner",""+temp[h]);
                    }
                    if(temp.length == 2){
                        Log.d("conner","2 byte "+act);
                        act = (temp[0] << 8)+(temp[1] & 0xff);
                    }else if(temp.length == 1){
                        Log.d("conner","1 byte");
                        if(temp[0] < 0){
                            act = (int)temp[0] + 256;
                        }else{
                            act = (int)temp[0];
                        }
                    }else if(temp.length ==3){
                        if(temp[2] < 0){
                            act = (int)temp[2] + 256;
                        }else{
                            act = (int)temp[2];
                        }
                    }

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable(){
                                @Override
                                public void run() {
                                 viewBeaocn1(act);
                                }
                            });
                        }
                    }).start();
                }

                //Log.d(TAG, "I see a beacon transmitting a url: " + url +
                //        " approximately " + beacon.getDistance() + " meters away.");
                //int start = url.indexOf('.');
                //int end = url.lastIndexOf('.');
                //String data = url.substring(start+1, end);
                //Log.d("conner",""+data);
                //byte[] nimi = url.getBytes();
                //int length = nimi.length;*/

            }
        }

    }

    private void viewBeaocn1(int act)
    {
        tvBe1Act.setText(""+act);
    }

    private void viewBeaocn2(int act)
    {
        tvBe2Act.setText(""+act);
    }


    private void viewBeaocn3(int act)
    {
        tvBe3Act.setText(""+act);
    }

}
